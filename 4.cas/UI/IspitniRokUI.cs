﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Modul1Termin04.Primer9.Samostalni;
using Modul1Termin04.Primer7.Utils;

namespace Modul1Termin04.Primer7.UI
{
    static class IspitniRokUI
    {
        /** ATRIBUTI KLASE ****/
        public static List<IspitniRok> ListaIspitnihRokova { get; set; }

        static IspitniRokUI()
        {
            ListaIspitnihRokova = new List<IspitniRok>();
        }

        /** MENI OPCJA ****/
        public static void MeniIspitniRokUI()
        {
            int odluka = -1;
            while (odluka != 0)
            {
                IspisiOpcijeIspitnihRokova();
                Console.Write("Opcija:");
                odluka = IOPomocnaKlasa.OcitajCeoBroj();
                Console.Clear();
                switch (odluka)
                {
                    case 0:
                        Console.WriteLine("Izlaz");
                        break;
                    case 1:
                        UnosNovogIspitnogRoka();
                        break;
                    case 2:
                        IzmenaPodatakaOIspitnomRoku();
                        break;
                    case 3:
                        // BrisanjePodatakaOStudentu();
                        break;
                    case 4:
                        IspisiSveIspitneRokove();
                        break;
                    case 5:
                        IspitniRok st = PronadjiIspitniRokPoNazivu();
                        if (st != null)
                        {
                            Console.WriteLine(st.ToStringAll());
                        }
                        break;
                    default:
                        Console.WriteLine("Nepostojeca komanda!\n\n");
                        break;
                }
            }
        }

        /** METODE ZA ISPIS OPCIJA ****/
        //ispis teksta osnovnih opcija

        public static void IspisiOpcijeIspitnihRokova()
        {
            Console.WriteLine("Rad sa ispitnim rokovima - opcije:");
            Console.WriteLine("\tOpcija broj 1 - unos podataka o novom ispitnom roku");
            Console.WriteLine("\tOpcija broj 2 - izmena podataka o ispitnom roku");
            Console.WriteLine("\tOpcija broj 3 - brisanje podataka o ispitnom roku");
            Console.WriteLine("\tOpcija broj 4 - ispis podataka svih ispitnih rokova");
            Console.WriteLine("\tOpcija broj 5 - ispis podataka o odredenom ispitnom roku po Id-");
            Console.WriteLine("\t\t ...");
            Console.WriteLine("\tOpcija broj 0 - POVRATAK NA GLAVNI MENI");
        }


        /** METODE ZA ISPIS ISPITNIH ROKOVA ****/
        //ispisi sve ispitne rokove
        public static void IspisiSveIspitneRokove()
        {
            for (int i = 0; i < ListaIspitnihRokova.Count; i++)
            {
                Console.WriteLine(ListaIspitnihRokova[i]);
            }
        }

        /** METODE ZA PRETRAGU ISPITNIH ROKOVA****/
        //pronadji ispitni rok
        public static IspitniRok PronadjiIspitniRokPoNazivu()
        {
            IspitniRok retVal = null;
            Console.WriteLine("Unesi naziv ispitnog roka:");
            String stNaziv = IOPomocnaKlasa.OcitajTekst();
            retVal = PronadjiIspitniRokPoNazivu(stNaziv);
            if (retVal == null)
                Console.WriteLine("Ispitni rok sa imenom " + stNaziv + " ne postoji u evidenciji");
            return retVal;
        }

        //pronadji studenta
        public static IspitniRok PronadjiIspitniRokPoNazivu(String stNaziv)
        {
            IspitniRok retVal = null;
            for (int i = 0; i < ListaIspitnihRokova.Count; i++)
            {
                IspitniRok st = ListaIspitnihRokova[i];
                if ((st.Naziv.ToUpper()).Equals(stNaziv.ToUpper()))
                {
                    retVal = st;
                    break;
                }
            }
            return retVal;
        }

        /** METODE ZA UNOS i IZMENU STUDENATA****/

        //unos novog ispitnog roka
        public static void UnosNovogIspitnogRoka()
        {
            Console.WriteLine("Unesi naziv roka:");
            String stNaziv = IOPomocnaKlasa.OcitajTekst();
            // stNaziv = stNaziv.ToUpper();
            while (PronadjiIspitniRokPoNazivu(stNaziv) != null)
            {
                Console.WriteLine("Ispitni rok sa imenom " + stNaziv + " vec postoji");
                stNaziv = IOPomocnaKlasa.OcitajTekst();
            }

            Console.WriteLine("Unesi pocetak roka:");
            String stPocetak = IOPomocnaKlasa.OcitajTekst();
            Console.WriteLine("Unesi kraj roka:");
            String stKraj = IOPomocnaKlasa.OcitajTekst();

            //ID atribut ce se dodeliti automatski
            IspitniRok st = new IspitniRok(stNaziv, stPocetak, stKraj);
            ListaIspitnihRokova.Add(st);
        }

        //izmena ispitnog roka
        public static void IzmenaPodatakaOIspitnomRoku()
        {
            IspitniRok st = PronadjiIspitniRokPoNazivu();
            if (st != null)
            {
                Console.WriteLine("Unesi Naziv:");
                String stNaziv = IOPomocnaKlasa.OcitajTekst();
                Console.WriteLine("Unesi pocetak roka:");
                String stPocetak = IOPomocnaKlasa.OcitajTekst();
                Console.WriteLine("Unesi kraj roka:");
                String stKraj = IOPomocnaKlasa.OcitajTekst();

                st.Naziv = stNaziv;
                st.Pocetak = stPocetak;
                st.Kraj = stKraj;
            }
        }

        /** METODA ZA UCITAVANJE PODATAKA****/
        public static void UcitajIspitneRokoveIzDatoteke(string nazivDatoteke)
        {
            if (File.Exists(nazivDatoteke))
            {
                using (StreamReader reader1 = File.OpenText(nazivDatoteke))
                {
                    string linija = "";
                    while ((linija = reader1.ReadLine()) != null)
                    {
                        ListaIspitnihRokova.Add(new IspitniRok(linija));
                    }
                }
            }
            else
            {
                Console.WriteLine("Datoteka" + nazivDatoteke + " ne postoji ili putanja nije ispravna.");
            }

        }
    }
}
