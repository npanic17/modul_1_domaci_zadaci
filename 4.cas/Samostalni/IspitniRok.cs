﻿/* ovo je 1. i 2.domaci zadatak iz termina 4.
 * 1.zad: Update ispitnaprijava i ispitni rok da lice na student klasu
 * 2.zad: prosiriti primer 7. koji ima UI deo klasa sa novim opcijama IspitnaPrijava i IspitniRok
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin04.Primer9.Samostalni
{
    class IspitniRok
    {
        private static int brojacId = 0;

        internal int BrojacId
        {
            get { return brojacId; }
            set { brojacId = value; }
        }
        internal int Id { get; set; }
        internal string Naziv { get; set; }
        internal string Pocetak { get; set; }
        internal string Kraj { get; set; }

        public List<IspitnaPrijava> IspitnePrijave { get; set; } = new List<IspitnaPrijava>();

        //konstruktori
        public IspitniRok()
        {

        }

        public IspitniRok(string naziv, string pocetak, string kraj, int id = -1)
        {
            if (id == -1)
            {
                id = brojacId++;
            }
            this.Id = id;
            this.Naziv = naziv;
            this.Pocetak = pocetak;
            this.Kraj = kraj;
        }

        public IspitniRok(string naziv, string pocetak, string kraj, List<IspitnaPrijava> ispitnePrijave, int id = -1) : this(naziv, pocetak, kraj, id)
        {
            this.IspitnePrijave = ispitnePrijave;
        }



        public IspitniRok(string tekst)
        {
            string[] tokeni = tekst.Split(',');
            //npr. 		1,Januarski,2015-01-15,2015-01-29
            //tokeni 	0		1		2		3		

            //TO DO
            if (tokeni.Length != 4)
            {
                Console.WriteLine("Greska pri ocitavanju ispitnih rokova " + tekst);
                //izlazak iz aplikacije
                Environment.Exit(0);
            }

            Id = Int32.Parse(tokeni[0]);
            Naziv = tokeni[1];
            Pocetak = tokeni[2];
            Kraj = tokeni[3];
        }

        //metode

        //kraci naziv metode PreuzmiTekstualnuReprezentacijuKlaseZaDatoteku
        //implementirati isto ponašanje
        public string ToFileString()
        {
            //TO DO
            StringBuilder sb = new StringBuilder();
            sb.Append(Id + "," + Naziv + "," + Pocetak + "," + Kraj);
            return sb.ToString();
        }

        public override string ToString()
        {
            return "Ispitni " + Naziv + " rok [id:" + Id + "] traje od " + Pocetak + " do " + Kraj;
        }

        //ispisati sve podatke sa ispitnim prijavama
        //po ugledu na metodu u klasi Predmet
        public string ToStringAll()
        {
            //TO DO

            StringBuilder sb = new StringBuilder("Ispitni " + Naziv + " rok[id: " + Id + "] traje od " + Pocetak + " do " + Kraj + "\n");

            if (IspitnePrijave.Count > 0)
            {
                sb.AppendLine("U ovom roku postoje sledece ispitne prijave: ");
                for (int i = 0; i < IspitnePrijave.Count; i++)
                {
                    sb.AppendLine("\t" + IspitnePrijave[i] + "\n");
                }
            }

            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is IspitniRok))
                return false;

            IspitniRok ir = (IspitniRok)obj;

            return this.Id.Equals(ir.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode() ^ Naziv.GetHashCode();
        }
    }
}
