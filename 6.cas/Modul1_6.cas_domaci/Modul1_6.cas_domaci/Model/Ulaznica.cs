﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_6.cas_domaci.Model
{
    class Ulaznica
    {
        public int Cena { get; set; }
        public string TipUlaznice { get; set; }
        public Dogadjaj Dogadjaj { get; set; }
        public Osoba Osoba { get; set; }

        public Ulaznica(int cena, string tipUlaznice, Dogadjaj dogadjaj, Osoba osoba)
        {
            Cena = cena;
            TipUlaznice = tipUlaznice;
            Dogadjaj = dogadjaj;
            Osoba = osoba;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("Cena: " + Cena + " Tip ulaznice: " + TipUlaznice);
            if (Dogadjaj != null) sb.AppendLine(Dogadjaj.ToString());
            if (Osoba != null) sb.AppendLine(Osoba.ToString());

            return sb.ToString();
        }
    }
}
