﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_6.cas_domaci.Model
{
    class Dogadjaj
    {
        public string Vreme { get; set; }
        public string MestoOdrzavanja { get; set; }

        public Dogadjaj(string vreme, string mestoOdrzavanja)
        {
            Vreme = vreme;
            MestoOdrzavanja = mestoOdrzavanja;
        }

        public override string ToString()
        {
            return "Vreme: " + Vreme + " Mesto: " + MestoOdrzavanja;
        }
    }


    class MuzickiDogadjaj : Dogadjaj
    {
        public string Izvodjac { get; set; }
        public string ZanrMuzike { get; set; }

        public MuzickiDogadjaj(string vreme, string mestoOdrzavanja) : base(vreme, mestoOdrzavanja)
        {
        }

        public MuzickiDogadjaj(string vreme, string mestoOdrzavanja, string izvodjac, string zanrMuzike) : base(vreme, mestoOdrzavanja)
        {
            Izvodjac = izvodjac;
            ZanrMuzike = zanrMuzike;
        }
        public override string ToString()
        {
            return base.ToString() + "Izvodjac: " + Izvodjac + " Zanr: " + ZanrMuzike;
        }
    }

    class SportskiDogadjaj : Dogadjaj
    {
        public string VrstaSporta { get; set; }

        public SportskiDogadjaj(string vreme, string mestoOdrzavanja) : base(vreme, mestoOdrzavanja)
        {
        }

        public SportskiDogadjaj(string vreme, string mestoOdrzavanja, string vrstaSporta) : base(vreme, mestoOdrzavanja)
        {
            VrstaSporta = vrstaSporta;
        }


        public override string ToString()
        {
            return base.ToString() + "Vrsta sporta: " + VrstaSporta;
        }

    }


}
