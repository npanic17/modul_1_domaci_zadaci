﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_6.cas_domaci.Model
{
    class Osoba
    {
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string JMBG { get; set; }

        public Osoba(string ime, string prezime, string jMBG)
        {
            Ime = ime;
            Prezime = prezime;
            JMBG = jMBG;
        }

        public override string ToString()
        {
            return "Ime: " + Ime + " Prezime: " + Prezime + "JMBG" + JMBG;
        }
    }
}
