﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak1
{
    class Program
    {
        static void Main(string[] args)
        {
            double Tezina = 20;
            string Primaoc = "Miki Manojlovic, bulevar oslobodjenja 12";
            string Posiljaoc = "Vuk Mandusic, sremska 4";
            string PreporucenoSlanje = "Da";

            Console.WriteLine("Posiljac "+Posiljaoc+" salje paket primaocu "+Primaoc);
            Console.WriteLine("Tezina paketa je "+Tezina);
            Console.WriteLine("Poslati preporuceno: "+PreporucenoSlanje);
        }
    }
}
