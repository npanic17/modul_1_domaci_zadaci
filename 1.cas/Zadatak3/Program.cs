﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3
{
    class Program
    {
        static void Main(string[] args)
        {
            double poluprecnik = 4;
            Console.WriteLine("Obim kruga je {0:f}",2*poluprecnik*Math.PI);
            Console.WriteLine("Povrsina kruga je {0:f}", poluprecnik * poluprecnik * Math.PI);
            Console.ReadKey();
        }
    }
}
