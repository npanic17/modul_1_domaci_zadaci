﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StampanjeDijamanta
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 5, nzv, nrz, korzv, korrz;
            nzv = 1;
            nrz = 2;
            korzv = 2;
            korrz = 1;
            for(int i = 0; i < n; i++)
            {
                for (int j = 1; j <= nrz; j++) Console.Write(" ");
                for (int j = 1; j <= nzv; j++) Console.Write("*");
                Console.WriteLine();
                if (nzv == n) { korzv = -2; korrz = -1; }
                    nzv += korzv; nrz -= korrz;
            }
            Console.ReadKey();
        }
    }
}
