﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DodatniZadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] oglasisifra = { "001", "002", "003" };
            string[] oglasinaslov = { "Prodajem punta", "Audi ko casa", "Uvoz iz svice" };
            string[] oglasicena = { "1100", "7000", "12500" };
            string[] oglasgodiste = { "2004", "2005", "2012" };
            string[][] oprema = {
                new string[] {"ABS", "ESP", "Alarm","AirBag","AutoKlima" },
                new string[] {"ABS", "ESP", "Alarm","AirBag","Klima" },
                new string[] {"ABS", "Maglenke", "Alarm","AirBag","Servo" }
            };

            //ispisa svih oglasa u formi: redni broj, šifra, naslov oglasa i cena.
            for (int i = 0; i < oglasisifra.Length; i++)
            {
                Console.WriteLine("Red broj: " + (i + 1) + "Sifra: " + oglasisifra[i] + "Naslov oglasa: " + oglasinaslov[i] + "Cena auta: " + oglasicena[i]);
                Console.WriteLine();
            }

            //ispis svih oglasa čija su vozila proizvedena u određenoj godini u formi: redni broj, šifra, naslov oglasa i cena.
            for (int i = 0; i < oglasgodiste.Length; i++)
            {
                if (oglasgodiste[i] == "2005")
                {
                    Console.WriteLine("Red broj: " + (i + 1) + "Sifra: " + oglasisifra[i] + "Naslov oglasa: " + oglasinaslov[i] + "Cena auta: " + oglasicena[i]);
                    Console.WriteLine("Sifra: " + oglasisifra[i]);
                    Console.WriteLine();
                }

            }

            //ispisa svih detalja o odeređenom oglasu u formi šifra, naslov oglasa, cena i oprema.
            Console.WriteLine("Red broj: 1");
            Console.WriteLine("Sifra: " + oglasisifra[0]);
            Console.WriteLine("Naslov oglasa: " + oglasinaslov[0]);
            Console.WriteLine("Cena auta: " + oglasicena[0]);
            Console.WriteLine("Godiste auta: " + oglasgodiste[0]);
            for (int i = 0; i < oprema[0].Length; i++)
            {
                Console.WriteLine("Oprema auta: " + oprema[0][i]);
            }


            Console.ReadKey();
        }
    }
}
