﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            double a = 3, b = 5;

            Console.WriteLine("Obim pravougaonika je {0:f}", 2*a+2*b);
            Console.WriteLine("Povrsina pravougaonika je {0:f}", a*b);
            Console.ReadKey();
        }
    }
}
