﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6
{
    class Program
    {
        static void Main(string[] args)
        {
            int godina = 2014;

            if (godina % 400 == 0) Console.WriteLine(godina+". je prestupna");
            else if (godina % 100 == 0) Console.WriteLine(godina + ". nije prestupna");
            else if (godina % 4 == 0) Console.WriteLine(godina + ". je prestupna");
            else Console.WriteLine(godina + ". nije prestupna");
        }

    }
}
