﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak8
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 4, y = 5;
            string operacija = "equals";
            switch (operacija)
            {
                case "min":
                    Console.WriteLine("Min broj:{0}",x<y?x:y);
                    break;
                case "max":
                    Console.WriteLine("Max broj:{0}", x > y ? x : y);
                    break;
                case "swap":
                    int temp = x; x = y; y = temp;
                    Console.WriteLine("nakon zamene x={0},y={1}", x, y);
                    break;
                case "equals":
                    Console.WriteLine("jednakost:{0}", x==y?0:x>y?1:-1);
                    break;
            }
            Console.ReadKey();
        }
    }
}
