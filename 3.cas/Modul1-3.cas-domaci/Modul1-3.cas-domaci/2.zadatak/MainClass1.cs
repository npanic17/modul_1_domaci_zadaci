﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_3.cas_domaci._2.zadatak
{
    class MainClass1
    {
        //ispis
        public static void IspisiStudente(List<Student> lista)
        {
            //ispis svih studenata I NACIN
            //for (int i = 0; i < lista.Count; i++)
            //{
            //    Student st = (Student)lista[i];
            //    Console.WriteLine(st.PreuzmiTekstualnuReprezentacijuKlase());
            //}

            //ispis svih studenata II NACIN
            foreach (Student st in lista)
            {
                Console.WriteLine(st.PreuzmiTekstualnuReprezentacijuKlase());
            }
        }

        public static void Main(String[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            List<Student> sviStudenti = new List<Student>();
            //1,E2 01/2016,Jevrić,Srđan,Loznica
            Student student1 = new Student(1, "Srđan", "Jevrić", "Loznica", "E2 01/2016");
            //2,E2 02/2016,Savić,Ana,Novi Sad
            Student student2 = new Student(2, "Ana", "Savić", "Novi Sad", "E2 02/2016");
            //3,E2 03/2016,Babić,Branko,Inđija
            Student student3 = new Student(3, "Branko", "Babić", "Inđija", "E2 03/2016");

            //dodavanje studenata
            sviStudenti.Add(student1);
            sviStudenti.Add(student2);
            sviStudenti.Insert(1, student3);

            //4,E1 01/2016,Sekulić,Miloš,Beograd
            sviStudenti.Insert(0, new Student(4, "Miloš ", "Sekulić", "Beograd", "E1 01/2016"));

            IspisiStudente(sviStudenti);

            Console.WriteLine("******************************");
            //uklanjanje elemenata iz liste, ne i brisanje
            sviStudenti.RemoveAt(2);
            Console.WriteLine("Broj studenata je:" + sviStudenti.Count);
            IspisiStudente(sviStudenti);

            //uklanjanje svih elemenata iz liste
            sviStudenti.Clear();
            Console.WriteLine("Broj studenata je:" + sviStudenti.Count);
            IspisiStudente(sviStudenti);
            Console.WriteLine("Zavrsen rad sa listom");

            //TO DO: Dodati predmete studentima i unutar svakog predmeta niz studenata koji pohađaju dati predmet

            //1,Januarski,2015-01-15,2015-01-29
            //2,Februarski,2015-02-01,2015-02-14
            List<IspitniRok> sviIspitniRokovi = new List<IspitniRok>()
            {
                new IspitniRok(1, "Januarski", "2015-01-15","2015-01-29"),
                new IspitniRok(2, "Februarski", "2015-02-01","2015-02-14"),
            };

            foreach (var el in sviIspitniRokovi) Console.WriteLine(el.PreuzmiTekstualnuReprezentacijuKlase());

            //lista iz csv ispitne prijave
            //1,1,1,88,89
            //1,2,2,85,55
            //2,2,2,80,45
            //3,1,1,94,96
            //4,1,1,40,60
            //4,1,2,83,88
            //4,2,2,89,91
            //4,4,2,100,98
            //5,2,1,45,47
            //5,3,2,56,55
            //6,3,2,25,0

            List<IspitnePrijave> sveprijave = new List<IspitnePrijave>
            {
                new IspitnePrijave(1, 1, sviIspitniRokovi[0], 88, 89),
                new IspitnePrijave(1, 1, sviIspitniRokovi[1], 85, 55),
                new IspitnePrijave(1, 1, sviIspitniRokovi[1], 80,45),
                new IspitnePrijave(1, 1, sviIspitniRokovi[0], 94,96),
                new IspitnePrijave(1, 1, sviIspitniRokovi[0], 40,60),
                new IspitnePrijave(1, 1, sviIspitniRokovi[1], 83,88),
                new IspitnePrijave(1, 1, sviIspitniRokovi[1], 89,91),
                new IspitnePrijave(1, 1, sviIspitniRokovi[1], 100,98),
                new IspitnePrijave(1, 1, sviIspitniRokovi[0], 45,47),
                new IspitnePrijave(1, 1, sviIspitniRokovi[1], 56,55),
                new IspitnePrijave(1, 1, sviIspitniRokovi[1], 25,0),
            };

            foreach (var el in sveprijave) Console.WriteLine(el.ToString());

            Console.ReadKey();
        }

    }
}
