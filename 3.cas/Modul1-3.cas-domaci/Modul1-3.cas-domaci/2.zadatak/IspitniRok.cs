﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_3.cas_domaci._2.zadatak
{
    class IspitniRok
    {
        private int _id;
        private string _naziv;
        private string _pocetak;
        private string _kraj;

        public int Id { get => _id; set => _id = value; }
        public string Naziv { get => _naziv; set => _naziv = value; }
        public string Pocetak { get => _pocetak; set => _pocetak = value; }
        public string Kraj { get => _kraj; set => _kraj = value; }

        public IspitniRok()
        {
        }

        public IspitniRok(int id, string naziv, string pocetak, string kraj)
        {
            Id = id;
            Naziv = naziv;
            Pocetak = pocetak;
            Kraj = kraj;
        }

        public string PreuzmiTekstualnuReprezentacijuKlase()
        {
            string s = "Id: " + Id + " Naziv: " + Naziv + " Pocetak: " + Pocetak + " Kraj: " + Kraj;
            return s;
        }

        public bool Isti(IspitniRok ir)
        {
            if (this.Id == ir.Id && this.Naziv == ir.Naziv && this.Pocetak == ir.Pocetak && this.Kraj == ir.Kraj)
                return true;
            else return false;
        }

        
    }
}
