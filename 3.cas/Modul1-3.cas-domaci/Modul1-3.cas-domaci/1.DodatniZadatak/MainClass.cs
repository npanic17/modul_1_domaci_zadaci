﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_3.cas_domaci._1.DodatniZadatak
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            List<Igrac> spisakIgraca = new List<Igrac>()
            {
                new Igrac(1,"Mile","Ilic"),
                new Igrac(2,"Jao","Ming"),
            };

            // foreach (var el in spisakIgraca) Console.WriteLine(el);

            


            Igrac Mile = new Igrac(1, "Mile", "Ilic");
            Klub Partizan = new Klub(1, "Partizan");
            Partizan.ListaIgraca = spisakIgraca;
            
            Klub Zvezda = new Klub(3, "Zvezda");
            Partizan.Protivnik = Zvezda;
            Mile.KlubIgraca = Partizan;

            Console.WriteLine(Mile);
            //Console.WriteLine(Partizan);

            List<Klub> spisakKlubova = new List<Klub>();
            spisakKlubova.Add(Partizan);
            spisakKlubova.Add(Zvezda);

            Console.ReadKey();
        }
    }
}
