﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_3.cas_domaci._1.DodatniZadatak
{
    class Utakmica
    {
        public int UtakmicaId { get; set; }
        public string UtakmicaDatumVreme { get; set; }
        public Klub KlubDomacin { get; set; }
        public Klub KlubGost { get; set; }
        public int DomacinBrKoseva { get; set; }
        public int GostBrKoseva { get; set; }
        List<Sudija> ListaSudija;
    }
}
