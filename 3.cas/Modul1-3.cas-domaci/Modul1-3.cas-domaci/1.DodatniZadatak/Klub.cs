﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_3.cas_domaci._1.DodatniZadatak
{
    public class Klub
    {
        public int KlubId { get; set; }
        public string KlubNaziv { get; set; }
        public List<Igrac> ListaIgraca { get; set; }
        
        public Klub Protivnik { get; set; }

        public Klub()
        {
            ListaIgraca = new List<Igrac>();
        }
        public Klub(int klubId, string klubNaziv)
        {
            KlubId = klubId;
            KlubNaziv = klubNaziv;
        }
        public Klub(int klubId, string klubNaziv, Klub protivnik)
        {
            KlubId = klubId;
            KlubNaziv = klubNaziv;
            ListaIgraca = new List<Igrac>(); 
            Protivnik = protivnik;
        }
        public Klub(int klubId, string klubNaziv, List<Igrac> listaIgraca, Klub protivnik)
        {
            KlubId = klubId;
            KlubNaziv = klubNaziv;
            ListaIgraca = listaIgraca;
            Protivnik = protivnik;
        }

        public string PreuzmiTekstualnuReprezentacijuKlase()
        {
            return "Klub: " + KlubNaziv;
        }

        public override string ToString()
        {
            StringBuilder s = new StringBuilder("Klub: " + KlubNaziv + " Protivnik: " + Protivnik.PreuzmiTekstualnuReprezentacijuKlase());

            if (ListaIgraca != null)
            {
                s.Append(" igraci kluba\n");
                for (int i = 0; i < ListaIgraca.Count; i++)
                {
                    s.Append("\t" + ListaIgraca[i].PreuzmiTekstualnuReprezentacijuKlase() + "\n");
                }
            }

            return s.ToString();

        }

        public override bool Equals(object obj)
        {
            Klub x = (Klub)obj;
            if (this.KlubId == x.KlubId)
                return true;
            else return false;
        }



    }
}
