﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_3.cas_domaci._1.DodatniZadatak
{
    public class Igrac
    {
        public int IgracId { get; set; }
        public string IgracIme { get; set; }
        public string IgracPrezime { get; set; }
        public Klub KlubIgraca { get; set; }

        public Igrac()
        {
            KlubIgraca = new Klub();
        }
        public Igrac(int igracId, string igracIme, string igracPrezime)
        {
            IgracId = igracId;
            IgracIme = igracIme;
            IgracPrezime = igracPrezime;
            KlubIgraca = new Klub();
        }
        public Igrac(int igracId, string igracIme, string igracPrezime, Klub klubIgraca)
        {
            IgracId = igracId;
            IgracIme = igracIme;
            IgracPrezime = igracPrezime;
            KlubIgraca = klubIgraca;
        }

        public string PreuzmiTekstualnuReprezentacijuKlase()
        {
            return "Ime i Prezime: " + IgracIme + " " + IgracPrezime;
        }
        public override string ToString()
        {
            
            return "Ime i Prezime: " + IgracIme + " " + IgracPrezime + " "+KlubIgraca.PreuzmiTekstualnuReprezentacijuKlase();
        }

        public override bool Equals(object obj)
        {
            Igrac x = (Igrac)obj;
            if (this.IgracId == x.IgracId)
                return true;
            else return false;
        }
    }
}
