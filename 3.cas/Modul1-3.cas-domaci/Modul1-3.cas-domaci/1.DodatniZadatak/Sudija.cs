﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_3.cas_domaci._1.DodatniZadatak
{
    class Sudija
    {
        public int SudijaId { get; set; }
        public string SudijaIme { get; set; }
        public string SudijaPrezime { get; set; }
    }
}
