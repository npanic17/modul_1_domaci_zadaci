﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_3.cas_domaci._3.zadatak
{
    class IspitnePrijave
    {
        public int Student { get; set; }
        public int Predmet { get; set; }
        private IspitniRok ispitniRok;
        public double BodoviTeorija { get; set; }
        public double BodoviZadaci { get; set; }

        public List<IspitniRok> IspitniRokovi { get; set; }
        public List<Student> SviStudenti { get; set; }
        public List<Predmet> SviPredmeti { get; set; }


        public IspitnePrijave()
        {
            ispitniRok = new IspitniRok();
        }

        public IspitnePrijave(int student, int predmet, double bodoviTeorija, double bodoviZadaci)
        {
            Student = student;
            Predmet = predmet;
            this.ispitniRok = ispitniRok;
            BodoviTeorija = bodoviTeorija;
            BodoviZadaci = bodoviZadaci;
            ispitniRok = new IspitniRok();
        }

        public IspitnePrijave(int student, int predmet, IspitniRok ispitniRok, double bodoviTeorija, double bodoviZadaci)
        {
            Student = student;
            Predmet = predmet;
            this.ispitniRok = ispitniRok;
            BodoviTeorija = bodoviTeorija;
            BodoviZadaci = bodoviZadaci;
        }

        public override string ToString()
        {
            string s = "Student: " + Student + " Predmet: " + Predmet + " Ispitni rok: " + ispitniRok.PreuzmiTekstualnuReprezentacijuKlase() + " Broj bodova na teoriji: " + BodoviTeorija + " Broj bodova na teoriji: " + BodoviZadaci;
            return s;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is IspitnePrijave))
                return false;
            IspitnePrijave s = (IspitnePrijave)obj;
            return this.Student == s.Student;
        }

        public void IzracunajOcenu()
        {
            double ocena = BodoviTeorija + BodoviZadaci;
            if (ocena > 92) Console.WriteLine("Dobili ste 10tku");
            else if(ocena>82 && ocena<=92) Console.WriteLine("Dobili ste 9tku");
            else if (ocena > 72 && ocena <= 82) Console.WriteLine("Dobili ste 8tku");
            else if (ocena > 62 && ocena <= 72) Console.WriteLine("Dobili ste 7tku");
            else if (ocena > 52 && ocena <= 62) Console.WriteLine("Dobili ste 6tku");
            else  Console.WriteLine("Niste polozili ispit");
        }

        public double IzracunajProsek()
        {
            return (BodoviZadaci + BodoviTeorija) / 2;
        }


    }
}
