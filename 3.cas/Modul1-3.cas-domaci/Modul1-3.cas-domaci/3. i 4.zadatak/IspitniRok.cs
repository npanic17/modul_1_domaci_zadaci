﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_3.cas_domaci._3.zadatak
{
    class IspitniRok
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public string Pocetak { get; set; }
        public string Kraj { get; set; }

        public List<IspitniRok> IspitniRokovi { get; set; }

        public IspitniRok()
        {
        }

        public IspitniRok(int id, string naziv, string pocetak, string kraj)
        {
            Id = id;
            Naziv = naziv;
            Pocetak = pocetak;
            Kraj = kraj;
        }

        public string PreuzmiTekstualnuReprezentacijuKlase()
        {
            string s = "Id: " + Id + " Naziv: " + Naziv + " Pocetak: " + Pocetak + " Kraj: " + Kraj;
            return s;
        }

        public bool Isti(IspitniRok ir)
        {
            if (this.Id == ir.Id && this.Naziv == ir.Naziv && this.Pocetak == ir.Pocetak && this.Kraj == ir.Kraj)
                return true;
            else return false;
        }


    }
}
