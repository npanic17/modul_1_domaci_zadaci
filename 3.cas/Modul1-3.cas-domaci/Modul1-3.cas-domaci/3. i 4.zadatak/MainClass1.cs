﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_3.cas_domaci._3.zadatak
{
    class MainClass1
    {
        //ispis
        #region metode za ispis sadrzaja klasa
        public static void IspisiStudente(List<Student> lista)
        {
            //ispis svih studenata I NACIN
            //for (int i = 0; i < lista.Count; i++)
            //{
            //    Student st = (Student)lista[i];
            //    Console.WriteLine(st.PreuzmiTekstualnuReprezentacijuKlase());
            //}

            //ispis svih studenata II NACIN
            foreach (Student st in lista)
            {
                Console.WriteLine(st.ToString());
            }
        }

        public static void IspisiPredmete(List<Predmet> lista)
        {
            foreach (Predmet st in lista)
            {
                Console.WriteLine(st.ToString());
            }
        }

        public static void IspisiNastavnike(List<Nastavnik> lista)
        {
            foreach (Nastavnik st in lista)
            {
                Console.WriteLine(st.ToString());
            }
        }

        public static void IspisiIspitnerokove(List<IspitniRok> lista)
        {
            foreach (IspitniRok st in lista)
            {
                Console.WriteLine(st.ToString());
            }
        }

        public static void IspisiIspitnePrijave(List<IspitnePrijave> lista)
        {
            foreach (IspitnePrijave st in lista)
            {
                Console.WriteLine(st.ToString());
            }
        }
        #endregion


        public static void Main(String[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            #region Kreiranje liste<> sa 4 studenata
            List<Student> sviStudenti = new List<Student>();
            //1,E2 01/2016,Jevrić,Srđan,Loznica
            //2,E2 02/2016,Savić,Ana,Novi Sad
            //3,E2 03/2016,Babić,Branko,Inđija
            Student student1 = new Student(1, "Srđan", "Jevrić", "Loznica", "E2 01/2016");
            Student student2 = new Student(2, "Ana", "Savić", "Novi Sad", "E2 02/2016");
            Student student3 = new Student(3, "Branko", "Babić", "Inđija", "E2 03/2016");

            //dodavanje studenata
            sviStudenti.Add(student1);
            sviStudenti.Add(student2);
            sviStudenti.Insert(1, student3);

            //4,E1 01/2016,Sekulić,Miloš,Beograd
            sviStudenti.Insert(0, new Student(4, "Miloš ", "Sekulić", "Beograd", "E1 01/2016"));
            #endregion
            #region operacije uklanjanja i brisanja cele List<>
           // IspisiStudente(sviStudenti);

            //Console.WriteLine("******************************");
            //uklanjanje elemenata iz liste, ne i brisanje
            sviStudenti.RemoveAt(2);
          //  Console.WriteLine("Broj studenata je:" + sviStudenti.Count);
          //  IspisiStudente(sviStudenti);

            //uklanjanje svih elemenata iz liste
            sviStudenti.Clear();
          //  Console.WriteLine("Broj studenata je:" + sviStudenti.Count);

        //    Console.WriteLine("Zavrsen rad sa listom");
         //   Console.WriteLine("******************************");
            #endregion

            #region Novo kreiranje liste<> sa 4 stud
            sviStudenti = new List<Student>
            {
                new Student(1, "Srđan", "Konstantin", "Loznica", "E2 01/2016"),
                new Student(2, "Ana", "Savić", "Novi Sad", "E2 02/2016"),
                new Student(3, "Branko", "Babić", "Inđija", "E2 03/2016"),
                new Student(4, "Miloš ", "Sekulić", "Beograd", "E1 01/2016"),
                new Student(5, "Vuk ", "Askin", "Novi Sad", "E1 01/2016"),
                new Student(6, "Marko ", "Klainić", "Sombor", "E1 01/2016"),
                new Student(7, "Marko ", "Panić", "Zrenjanin", "E1 01/2016"),
            };
            #endregion

          

            //TO DO: Dodati predmete studentima i unutar svakog predmeta niz studenata koji pohađaju dati predmet
            #region Kreiranje liste<> sa 4 predmeta
            List<Predmet> SviPredmeti = new List<Predmet>
            {
                new Predmet(1,"Matematika"),
                new Predmet(2,"Fizika"),
                new Predmet(3,"Elektrotehnika"),
                new Predmet(4,"Informatika"),
            };
            #endregion

            #region dodavanje listi studenata po listu sa predmetima
            //assign 1.studentu 3 predmeta prema pohadja.csv
            for (int i = 0; i < 3; i++) sviStudenti[0].Predmeti.Add(SviPredmeti[i]);

            //assign 2.studentu 2. predmet prema pohadja.csv
            sviStudenti[1].Predmeti.Add(SviPredmeti[1]);

            //assign 3.studentu 1. predmet prema pohadja.csv
            sviStudenti[2].Predmeti.Add(SviPredmeti[0]);

            //assign 4.studentu 3 predmeta prema pohadja.csv
            sviStudenti[3].Predmeti.Add(SviPredmeti[0]);
            sviStudenti[3].Predmeti.Add(SviPredmeti[2]);
            sviStudenti[3].Predmeti.Add(SviPredmeti[3]);

            //assign 5.studentu 2 predmeta prema pohadja.csv
            for (int i = 0; i < 2; i++) sviStudenti[4].Predmeti.Add(SviPredmeti[i]);

            //assign 6.studentu 1. predmet prema pohadja.csv
            sviStudenti[5].Predmeti.Add(SviPredmeti[2]);
            #endregion

            #region dodavanje listi predmeta po listu sa studentima koji slusaju pojedinacne predmete
            //assign 1.predmetu 4 studenta prema pohadja.csv
            SviPredmeti[0].Studenti.Add(sviStudenti[0]);
            SviPredmeti[0].Studenti.Add(sviStudenti[2]);
            SviPredmeti[0].Studenti.Add(sviStudenti[3]);
            SviPredmeti[0].Studenti.Add(sviStudenti[4]);

            //assign 2.predmetu 3 studenta prema pohadja.csv
            SviPredmeti[1].Studenti.Add(sviStudenti[0]);
            SviPredmeti[1].Studenti.Add(sviStudenti[1]);
            SviPredmeti[1].Studenti.Add(sviStudenti[4]);

            //assign 3.predmetu 3 studenta prema pohadja.csv
            SviPredmeti[2].Studenti.Add(sviStudenti[0]);
            SviPredmeti[2].Studenti.Add(sviStudenti[3]);
            SviPredmeti[2].Studenti.Add(sviStudenti[5]);

            //assign 4.predmetu 1 studenta prema pohadja.csv
            SviPredmeti[3].Studenti.Add(sviStudenti[3]);
            #endregion



            #region lista 2 ispitna roka
            //1,Januarski,2015-01-15,2015-01-29
            //2,Februarski,2015-02-01,2015-02-14

            List<IspitniRok> sviIspitniRokovi = new List<IspitniRok>()
            {
                new IspitniRok(1, "Januarski", "2015-01-15","2015-01-29"),
                new IspitniRok(2, "Februarski", "2015-02-01","2015-02-14"),
            };


           // foreach (var el in sviIspitniRokovi) Console.WriteLine(el.PreuzmiTekstualnuReprezentacijuKlase());
            #endregion

            #region kreiranje liste ispitnih prijava
            //lista iz csv ispitne prijave
            //1,1,1,88,89
            //1,2,2,85,55
            //2,2,2,80,45
            //3,1,1,94,96
            //4,1,1,40,60
            //4,1,2,83,88
            //4,2,2,89,91
            //4,4,2,100,98
            //5,2,1,45,47
            //5,3,2,56,55
            //6,3,2,25,0

            List<IspitnePrijave> sveprijave = new List<IspitnePrijave>
            {
                new IspitnePrijave(1, 1, sviIspitniRokovi[0], 88, 89),
                new IspitnePrijave(1, 1, sviIspitniRokovi[1], 85, 55),
                new IspitnePrijave(1, 1, sviIspitniRokovi[1], 80,45),
                new IspitnePrijave(1, 1, sviIspitniRokovi[0], 94,96),
                new IspitnePrijave(1, 1, sviIspitniRokovi[0], 40,60),
                new IspitnePrijave(1, 1, sviIspitniRokovi[1], 83,88),
                new IspitnePrijave(1, 1, sviIspitniRokovi[1], 89,91),
                new IspitnePrijave(1, 1, sviIspitniRokovi[1], 100,98),
                new IspitnePrijave(1, 1, sviIspitniRokovi[0], 45,47),
                new IspitnePrijave(1, 1, sviIspitniRokovi[1], 56,55),
                new IspitnePrijave(1, 1, sviIspitniRokovi[1], 25,0),
            };

         //   foreach (var el in sveprijave) Console.WriteLine(el.ToString());
            #endregion

            #region kreiranje liste nastavnika
            //1,Petar,Petrović,Docent
            //2,Jovan,Jovanović,Docent
            //3,Marko,Marković,Asistent
            //4,Nikola,Nikolić,Redovni Profesor
            //5,Lazar,Lazić,Asistent
            List<Nastavnik> listanastavnika = new List<Nastavnik>()
            {
                new Nastavnik(1,"Petar","Petrović","Docent"),
                new Nastavnik(2,"Jovan","Jovanovic","Docent"),
                new Nastavnik(3,"Marko","Markovic","Asistent"),
                new Nastavnik(4,"Nikola","Nikolic","Redovni Profesor"),
                new Nastavnik(5,"Lazar","Lazić","Asistent"),
            };

            //foreach (var el in listanastavnika) Console.WriteLine(el.ToString());
            #endregion

           // IspisiStudente(sviStudenti);
           IspisiPredmete(SviPredmeti);

            Console.ReadKey();
        }

    }
}
