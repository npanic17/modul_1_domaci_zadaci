﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_3.cas_domaci._3.zadatak
{
    class Nastavnik
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Zvanje { get; set; }

        public Nastavnik()
        {

        }

        public Nastavnik(int id, string ime, string prezime, string zvanje)
        {
            Id = id;
            Ime = ime;
            Prezime = prezime;
            Zvanje = zvanje;
        }

        //konstruktor koji popunjava podatke na osnovu očitanog teksta iz fajla nastavnici.csv
        public Nastavnik(String tekst)
        {
            String[] tokeni = tekst.Split(',');
            //npr. 		1,Petar,Petrović,Docent
            //tokeni 	0		1		2		3			

            if (tokeni.Length != 4)
            {
                Console.WriteLine("Greska pri ocitavanju nastavnika " + tekst);
                //izlazak iz aplikacije
                Environment.Exit(0);
            }

            Id = Int32.Parse(tokeni[0]);
            Ime = tokeni[1];
            Prezime = tokeni[2];
            Zvanje = tokeni[3];
        }

        public override string ToString()
        {
            string s = "Id: " + Id + " Nastavnik: " + Ime + " " + Prezime + " Zvanje: " + Zvanje;
            return s;
        }

        //dva objekta su ista ako imaju isti id
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Nastavnik))
                return false;
            Nastavnik s = (Nastavnik)obj;
            return this.Id == s.Id;
        }

    }
}
