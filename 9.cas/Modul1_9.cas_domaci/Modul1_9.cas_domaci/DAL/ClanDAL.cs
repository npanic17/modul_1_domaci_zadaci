﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modul1_9.cas_domaci.Model;
using System.Data.SqlClient;

namespace Modul1_9.cas_domaci.DAL
{
    class ClanDAL
    {


        public static List<Clan> UzmiSveClanove(SqlConnection con)
        {
            List<Clan> ListaSvihClanova = new List<Clan>();
            string query = "SELECT Id_clana, Ime, Prezime FROM Termin09Biblioteka_clan";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                int Id = (int)rdr[0];
                string Ime = rdr[1].ToString();
                string Prezime = rdr[2].ToString();

                Clan NoviClan = new Clan(Id, Ime, Prezime);
                ListaSvihClanova.Add(NoviClan);
            }
            rdr.Close();

            return ListaSvihClanova;
        }

        public static void UpisNovogClana(SqlConnection con, string ime, string prezime)
        {
            string query = "INSERT INTO Termin09Biblioteka_clan(Ime,Prezime) VALUES (@ime,@prezime);";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@ime", ime);
            cmd.Parameters.AddWithValue("@prezime", prezime);
            cmd.ExecuteNonQuery();

        }

        //broj knjiga koji neki clan ima kod sebe
        public static int BrojKnjigaClana(SqlConnection conn, int _clanid)
        {
            //SELECT COUNT(Godina) FROM Termin09Biblioteka_knjiga WHERE Godina = '1954';
            string query = "SELECT COUNT(ClanId) FROM Termin09Biblioteka_knjiga WHERE ClanId = @index;";
            SqlCommand cmd = new SqlCommand(query,conn);
            cmd.Parameters.AddWithValue("@index", _clanid);
            
            return (int)cmd.ExecuteScalar(); 
        }

        //get clan po imenu i prezimenu! 
        public static Clan GetClanByImePrezime(SqlConnection con, string ime, string prezime)
        {
            string query = "Select * from Termin09Biblioteka_clan where Ime=@ime AND Prezime=@prezime";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@ime", ime);
            cmd.Parameters.AddWithValue("@prezime", prezime);
            SqlDataReader rdr = cmd.ExecuteReader();
            Clan Clan = null;

            while (rdr.Read())
            {
                int Id = (int)rdr[0];
                string Ime = rdr[1].ToString();
                string Prezime = rdr[2].ToString();
                Clan = new Clan(Id, Ime, Prezime);
            }

            return Clan;

        }

        //clan ima/nema knjigu! 
        public static bool GetClanByImePrezimeSaKnjigom(SqlConnection con, string ime, string prezime)
        {
            string query = "SELECT * FROM [DotNetKurs].[dbo].[Termin09Biblioteka_clan] AS Clan JOIN [DotNetKurs].[dbo].[Termin09Biblioteka_knjiga] AS Knjiga" +
" ON Clan.Id_clana = Knjiga.ClanId WHERE Clan.Ime = @ime AND Clan.Prezime = @prezime";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@ime", ime);
            cmd.Parameters.AddWithValue("@prezime", prezime);
            SqlDataReader rdr = cmd.ExecuteReader();
            Clan Clan = null;

            while (rdr.Read())
            {
                int Id = (int)rdr[0];
                string Ime = rdr[1].ToString();
                string Prezime = rdr[2].ToString();
                Clan = new Clan(Id, Ime, Prezime);
            }

            if (Clan == null) return false;
            else return true;
            

        }

        public static Clan GetClanById(SqlConnection con, int _Id)
        {
            string query = "Select * from Termin09Biblioteka_clan where Id_clana=@id";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@id", _Id);
            SqlDataReader rdr = cmd.ExecuteReader();
            Clan Clan = null;

            while (rdr.Read())
            {
                int Id = (int)rdr[0];
                string Ime = rdr[1].ToString();
                string Prezime = rdr[2].ToString();
                Clan = new Clan(Id, Ime, Prezime);
            }

            return Clan;

        }

        //brisanje clana koja nema referencu na knjigu
        public static void ObrisiClana(SqlConnection con, int _clanid)
        {
            string query = "DELETE FROM [DotNetKurs].[dbo].[Termin09Biblioteka_clan] where Id_clana=@clanid;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@clanid", _clanid);
            int n = cmd.ExecuteNonQuery();
            if (n != 0) Console.WriteLine("Uspesno izvrsena operacija brisanja clana.");
            else Console.WriteLine("Operacija brisanja nije uspesno zavrsena");
        }

    }
}
