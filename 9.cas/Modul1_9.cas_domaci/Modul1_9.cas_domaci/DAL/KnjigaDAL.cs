﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modul1_9.cas_domaci.Model;
using System.Data.SqlClient;

namespace Modul1_9.cas_domaci.DAL
{
    class KnjigaDAL
    {
        public static List<Knjiga> UzmiSveKnjige(SqlConnection con)
        {
            List<Knjiga> ListaSvihKnjiga = new List<Knjiga>();
            string query = " SELECT * FROM Termin09Biblioteka_knjiga";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                int Id = (int)rdr[0];
                string Ime = rdr[1].ToString();
                string Autor = rdr[2].ToString();
                string Godina = rdr[3].ToString();

                Clan Clan = null;

                //preuzimanje clanova koji su uzeli knjige
                if (rdr[4] != DBNull.Value)
                {
                    string query2 = " SELECT * FROM (Termin09Biblioteka_clan join Termin09Biblioteka_knjiga ON Termin09Biblioteka_clan.Id_clana = Termin09Biblioteka_knjiga.ClanID) WHERE Termin09Biblioteka_clan.Id_clana =" + (int)rdr[4];
                    SqlCommand cmd2 = new SqlCommand(query2, con);
                    SqlDataReader rdr2 = cmd2.ExecuteReader();
                    while (rdr2.Read())
                    {
                        int IdClan = (int)rdr2[0];
                        string ImeClana = rdr2[1].ToString();
                        string PrezimeClana = rdr2[2].ToString();
                        Clan = new Clan(IdClan, ImeClana, PrezimeClana);
                    }
                    rdr2.Close();
                }

                Knjiga NovaKnjiga = new Knjiga(Id, Ime, Autor, Godina, Clan);
                ListaSvihKnjiga.Add(NovaKnjiga);
            }
            rdr.Close();

            return ListaSvihKnjiga;
        }


        public static List<Knjiga> GetKnjiguNaslov(SqlConnection con, string naslov)
        {
            List<Knjiga> ListaSvihKnjiga = new List<Knjiga>();
            string query = "SELECT * FROM [DotNetKurs].[dbo].[Termin09Biblioteka_knjiga] WHERE Ime LIKE @index";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@index", "%"+naslov+"%");
            SqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                int Id = (int)rdr[0];
                string Ime = rdr[1].ToString();
                string Autor = rdr[2].ToString();
                string Godina = rdr[3].ToString();
                Knjiga NovaKnjiga = new Knjiga(Id, Ime, Autor, Godina);
                ListaSvihKnjiga.Add(NovaKnjiga);
            }
            return ListaSvihKnjiga;
        }

        public static Knjiga GetKnjiguNaslovAutor(SqlConnection con, string naslov, string autor)
        {
            string query = "Select * from Termin09Biblioteka_knjiga where Ime=@naslov AND Autor=@autor";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@naslov", naslov);
            cmd.Parameters.AddWithValue("@autor", autor);
            SqlDataReader rdr = cmd.ExecuteReader();
            Knjiga Knjiga = null;

            while (rdr.Read())
            {
                int Id = (int)rdr[0];
                string Ime = rdr[1].ToString();
                string Autor = rdr[2].ToString();
                string Godina = rdr[3].ToString();
                int ClanId = (rdr[4] != DBNull.Value ? (int)rdr[4] : 0);

                Knjiga = new Knjiga(Id, Ime, Autor, Godina, ClanDAL.GetClanById(Program.conn, ClanId));
            }

            return Knjiga;
        }

        public static void UpisNoveKnjige(SqlConnection con, string naslov, string autor, string godina)
        {
            string query = "INSERT INTO Termin09Biblioteka_knjiga(Ime,Autor,Godina) VALUES (@naslov,@autor,@godina);";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@naslov", naslov);
            cmd.Parameters.AddWithValue("@autor", autor);
            cmd.Parameters.AddWithValue("@godina", godina);
            int n = cmd.ExecuteNonQuery();
            if (n != 0) Console.WriteLine("Uspesno izvrsena operacija upisa nove knjige.");
            else Console.WriteLine("Operacija upisa nove knjige nije uspesno zavrsena");

        }


        public static void UpisIzdavanjaKnjige(SqlConnection con, int _clanid, int _knjigaid)
        {
            string query = "UPDATE [DotNetKurs].[dbo].[Termin09Biblioteka_knjiga] SET ClanId=@clanid where Id_knjige=@knjigaid;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@clanid", _clanid);
            cmd.Parameters.AddWithValue("@knjigaid", _knjigaid);
            int n = cmd.ExecuteNonQuery();
            if (n != 0) Console.WriteLine("Uspesno izvrsena operacija izdavanja knjige.");
            else Console.WriteLine("Operacija izdavanja nije uspesno zavrsena");
        }

        public static void VracanjeKnjige(SqlConnection con, int _knjigaid)
        {
            string query = "UPDATE [DotNetKurs].[dbo].[Termin09Biblioteka_knjiga] SET ClanId=NULL where Id_knjige=@knjigaid;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@knjigaid", _knjigaid);
            int n = cmd.ExecuteNonQuery();
            if (n != 0) Console.WriteLine("Uspesno izvrsena operacija vracanja knjige.");
            else Console.WriteLine("Operacija vracanja nije uspesno zavrsena");
        }


        //brisanje knjige koja nema referencu na clana
        public static void ObrisiKnjigu(SqlConnection con, int _knjigaid)
        {
            string query = "DELETE FROM [DotNetKurs].[dbo].[Termin09Biblioteka_knjiga] where Id_knjige=@knjigaid;";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@knjigaid", _knjigaid);
            int n = cmd.ExecuteNonQuery();
            if (n != 0) Console.WriteLine("Uspesno izvrsena operacija brisanja knjige.");
            else Console.WriteLine("Operacija brisanja nije uspesno zavrsena");
        }
    }
}
