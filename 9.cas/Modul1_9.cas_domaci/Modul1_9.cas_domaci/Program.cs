﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Modul1_9.cas_domaci.UI;
using Modul1_9.cas_domaci.Utils;

namespace Modul1_9.cas_domaci
{
    class Program
    {
        public static SqlConnection conn;
        static void LoadKonekciju()
        {
            try
            {
                string conPar = "Data Source =.\\SQLEXPRESS; Initial Catalog = DotNetKurs; Integrated Security = True; MultipleActiveResultSets = True";
                conn = new SqlConnection(conPar);
                conn.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static void UcitajMainMenu()
        {
            Console.Clear();
            Console.WriteLine("Program Biblioteka. Unesite neku od navedenih opcija");
            Console.WriteLine("Opcija 1-Rad sa clanovima");
            Console.WriteLine("Opcija 2-Rad sa knjigama");
            Console.WriteLine("--------------------------");
            Console.WriteLine("Opcija 0-Izlaz iz programa!");
        }

        static void Main(string[] args)
        {
            LoadKonekciju();

            int odluka = -1;
            while (odluka != 0)
            {
                UcitajMainMenu();
                odluka = IO.CitajCeoBroj();
                switch (odluka)
                {
                    case 0:
                        conn.Close();
                        break;
                    case 1:
                        ClanUI.ClanGUI();
                        break;
                    case 2:
                        KnjigaUI.KnjigaGUI();
                        break;
                    default:
                        Console.WriteLine("Nepostojeca opcija");
                        break;
                }
            }


        }
    }
}
