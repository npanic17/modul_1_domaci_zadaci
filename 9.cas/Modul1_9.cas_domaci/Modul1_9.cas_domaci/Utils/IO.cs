﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_9.cas_domaci.Utils
{
    public static class IO
    {
        public static int CitajCeoBroj()
        {
            Console.WriteLine("Unesite broj?");
            int broj;
            while (!Int32.TryParse(Console.ReadLine(), out broj))
            {
                Console.Write("Unesite opet broj:");
            }
            return broj;
        }
    }
}
