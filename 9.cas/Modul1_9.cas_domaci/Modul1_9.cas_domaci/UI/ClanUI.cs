﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modul1_9.cas_domaci.Model;
using Modul1_9.cas_domaci.Utils;
using Modul1_9.cas_domaci.DAL;
using Modul1_9.cas_domaci;

namespace Modul1_9.cas_domaci.UI
{
    class ClanUI
    {

        //ispis opcija za Clanove
        public static void UcitajClanUIMeni()
        {

            Console.WriteLine("Rad sa clanovima. Unesite neku od navedenih opcija");
            Console.WriteLine("Opcija 1-Ispis svih clanova biblioteke");
            Console.WriteLine("Opcija 2-Unos novog clana");
            Console.WriteLine("Opcija 3-Brisanje clana");
            Console.WriteLine("--------------------------");
            Console.WriteLine("Opcija 0-Izlaz");
        }

        //ClanUI
        public static void ClanGUI()
        {
            int odluka = -1;

            while (odluka != 0)
            {
                UcitajClanUIMeni();
                odluka = IO.CitajCeoBroj();
                switch (odluka)
                {
                    case 0:
                        break;
                    case 1:
                        IspisSvihClanova();
                        break;
                    case 2:
                        UnosNovogClana();
                        break;
                    case 3:
                        BrisanjeClana();
                        break;
                    default:
                        Console.WriteLine("Nepostojeca opcija");
                        break;
                }
            }
        }

        public static void IspisSvihClanova()
        {
            List<Clan> ListaClanova = new List<Clan>();
            ListaClanova = ClanDAL.UzmiSveClanove(Program.conn);
            foreach (Clan el in ListaClanova)
            {
                Console.WriteLine(el);
            }
        }

        public static void UnosNovogClana()
        {
            Console.Write("Unesite Ime novog clana:");
            string ime = Console.ReadLine();
            Console.Write("Unesite Prezime novog clana:");
            string prezime = Console.ReadLine();
            while (PronadjiClanaPoImePrezime(ime, prezime) != null)
            {
                Console.WriteLine("Clan sa ovim imenom i prezimenom vec postoji. Unesite drugo ime/prezime");
                Console.Write("Unesite Ime novog clana:");
                ime = Console.ReadLine();
                Console.Write("Unesite Prezime novog clana:");
                prezime = Console.ReadLine();
            }
            ClanDAL.UpisNovogClana(Program.conn, ime, prezime);
        }

        public static Clan PronadjiClanaPoImePrezime(string ime, string prezime)
        {
            return ClanDAL.GetClanByImePrezime(Program.conn, ime, prezime);
        }

        public static Clan PronadjiClanaPoImePrezime()
        {
            Console.Write("Unesite Ime clana:");
            string ime = Console.ReadLine();
            Console.Write("Unesite Prezime clana:");
            string prezime = Console.ReadLine();
            Clan clan = null;
            while ((clan=PronadjiClanaPoImePrezime(ime, prezime)) == null)
            {
                Console.WriteLine("Clan sa ovim imenom i prezimenom ne postoji. Unesite drugo ime/prezime");
                Console.Write("Unesite Ime clana:");
                ime = Console.ReadLine();
                Console.Write("Unesite Prezime clana:");
                prezime = Console.ReadLine();
            }
            return clan;
        }

        public static bool PostojiClanPoImePrezimeSaKnjigom(string ime, string prezime)
        {
            return ClanDAL.GetClanByImePrezimeSaKnjigom(Program.conn, ime, prezime);
        }


        public static void BrisanjeClana()
        {
            Clan clan = PronadjiClanaPoImePrezime();
            if (!PostojiClanPoImePrezimeSaKnjigom(clan.Ime, clan.Prezime))
            {
                ClanDAL.ObrisiClana(Program.conn, clan.Id);

            }
            else
            {
                Console.WriteLine("Clana nije moguce obrisati, posto u evidenciji stoji da je zaduzio knjigu(e)!");
            }

        }

    }
}
