﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modul1_9.cas_domaci.Model;
using Modul1_9.cas_domaci.Utils;
using Modul1_9.cas_domaci.DAL;
using Modul1_9.cas_domaci;

namespace Modul1_9.cas_domaci.UI
{
    class KnjigaUI
    {
        //ispis opcija za Clanove
        public static void UcitajKnjigaUIMeni()
        {
            Console.WriteLine("Rad sa knjigama. Unesite neku od navedenih opcija");
            Console.WriteLine("Opcija 1-Ispis svih knjiga biblioteke");
            Console.WriteLine("Opcija 2-Unos nove knjige u biblioteku");
            Console.WriteLine("Opcija 3-Izdavanje knjige clanu");
            Console.WriteLine("Opcija 4-Vracanje knjige u biblioteku");
            Console.WriteLine("Opcija 5-Brisanje knjige");
            Console.WriteLine("Opcija 6-Pretraga knjige po nazivu");
            Console.WriteLine("--------------------------");
            Console.WriteLine("Opcija 0-Izlaz");
        }

        //KnjigaUI
        public static void KnjigaGUI()
        {
            int odluka = -1;

            while (odluka != 0)
            {
                UcitajKnjigaUIMeni();
                odluka = IO.CitajCeoBroj();
                switch (odluka)
                {
                    case 0:
                        break;
                    case 1:
                        IspisSvihKnjiga();
                        break;
                    case 2:
                        UnosNoveKnjige();
                        break;
                    case 3:
                        IzdavanjeKnjige();
                        break;
                    case 4:
                        VracanjeKnjige();
                        break;
                    case 5:
                        BrisanjeKnjige();
                        break;
                    case 6:
                        PronadjiKnjiguPoNazivu();
                        Console.ReadKey();
                        break;
                    default:
                        Console.WriteLine("Nepostojeca opcija");
                        break;
                }
            }
        }

        //ispis svih knjiga
        public static void IspisSvihKnjiga()
        {
            List<Knjiga> ListaKnjiga = new List<Knjiga>();
            ListaKnjiga = KnjigaDAL.UzmiSveKnjige(Program.conn);
            foreach (Knjiga el in ListaKnjiga)
            {
                Console.WriteLine(el);
            }
        }

        //unos nove knjige u bazu
        public static void UnosNoveKnjige()
        {
            Console.Write("Unesite naslov knjige:");
            string naslov = Console.ReadLine();
            Console.Write("Unesite autora knjige:");
            string autor = Console.ReadLine();
            Console.Write("Unesite godinu izdavanja knjige:");
            string godina = Console.ReadLine();

            while (PronadjiKnjiguPoNazivuAutoru(naslov, autor) != null)
            {
                Console.WriteLine("Knjiga sa ovim nazivom i autorom vec postoji. Unesite drugo naziv/autor");
                Console.Write("Unesite novi naziv:");
                naslov = Console.ReadLine();
                Console.Write("Unesite novog autora:");
                autor = Console.ReadLine();
            }

            KnjigaDAL.UpisNoveKnjige(Program.conn, naslov, autor, godina);
        }

        //pronadji knjigu po nazivu 
        public static void PronadjiKnjiguPoNazivu()
        {
            Console.WriteLine("Unesite naslov knjige ili deo naslova");
            string naslov = Console.ReadLine();
            List<Knjiga> ListaKnjiga = new List<Knjiga>();
            ListaKnjiga = KnjigaDAL.GetKnjiguNaslov(Program.conn, naslov);
            foreach (Knjiga el in ListaKnjiga)
            {
                Console.WriteLine(el);
            }
        }

        //pronadji knjigu po nazivu i autoru
        public static Knjiga PronadjiKnjiguPoNazivuAutoru(string naslov, string autor)
        {
            return KnjigaDAL.GetKnjiguNaslovAutor(Program.conn, naslov, autor);
        }

        //pronadji knjigu po nazivu i autoru
        public static Knjiga PronadjiKnjiguPoNazivuAutoru()
        {
            Console.Write("Unesite naslov knjige:");
            string naslov = Console.ReadLine();
            Console.Write("Unesite autora knjige:");
            string autor = Console.ReadLine();
            Knjiga knjiga = null;
            while ((knjiga = PronadjiKnjiguPoNazivuAutoru(naslov, autor)) == null)
            {
                Console.WriteLine("Knjiga sa ovim nazivom i autorom nije pronadjena. Unesite opet naziv/autor");
                Console.Write("Unesite novi naziv:");
                naslov = Console.ReadLine();
                Console.Write("Unesite novog autora:");
                autor = Console.ReadLine();
            }
            return knjiga;

        }

        //izdavanje knjige clanu
        public static void IzdavanjeKnjige()
        {
            Knjiga knjiga = PronadjiKnjiguPoNazivuAutoru();

            if (knjiga.Clan == null)
            {
                SKOK:
                Console.Write("Unesite Ime clana kome ce se izdati knjiga:");
                string ime = Console.ReadLine();
                Console.Write("Unesite Prezime clana kome ce se izdati knjiga:");
                string prezime = Console.ReadLine();
                Clan clan = ClanUI.PronadjiClanaPoImePrezime(ime, prezime);

                //while (true)
                //{
                //    int x = ClanDAL.BrojKnjigaClana(Program.conn, clan.Id);

                //    if (clan != null && x<3) break;
                //    else if (clan == null)
                //    {
                //        Console.WriteLine("Clan sa ovim imenom i prezimenom nije pronadjen. Unesite opet ime/prezime");
                //        Console.Write("Unesite Ime clana kome ce se izdati knjiga:");
                //        ime = Console.ReadLine();
                //        Console.Write("Unesite Prezime clana kome ce se izdati knjiga:");
                //        prezime = Console.ReadLine();
                //        clan = ClanUI.PronadjiClanaPoImePrezime(ime, prezime);
                //    } else
                //    {
                //        Console.WriteLine("Clan sa ovim imenom i prezimenom ima max dozvoljen broj knjiga.");
                //        Console.Write("Unesite Ime clana kome ce se izdati knjiga:");
                //        ime = Console.ReadLine();
                //        Console.Write("Unesite Prezime clana kome ce se izdati knjiga:");
                //        prezime = Console.ReadLine();
                //        clan = ClanUI.PronadjiClanaPoImePrezime(ime, prezime);
                //    }

                //}

                while (((clan = ClanUI.PronadjiClanaPoImePrezime(ime, prezime)) == null))
                {
                    Console.WriteLine("Clan sa ovim imenom i prezimenom nije pronadjen. Unesite opet ime/prezime");
                    Console.Write("Unesite Ime clana kome ce se izdati knjiga:");
                    ime = Console.ReadLine();
                    Console.Write("Unesite Prezime clana kome ce se izdati knjiga:");
                    prezime = Console.ReadLine();
                }

                if (ClanDAL.BrojKnjigaClana(Program.conn, clan.Id) < 3)
                {
                    KnjigaDAL.UpisIzdavanjaKnjige(Program.conn, clan.Id, knjiga.Id);
                }
                else
                {
                    Console.WriteLine("Clan sa ovim imenom i prezimenom ima max dozvoljen broj knjiga.");
                    goto SKOK;
                }




            }
            else
            {
                Console.WriteLine("Knjiga je vec izdata!");
            }
        }

        //vracanje knjige u biblioteku
        public static void VracanjeKnjige()
        {
            Knjiga knjiga = PronadjiKnjiguPoNazivuAutoru();

            if (knjiga.Clan != null)
            {
                KnjigaDAL.VracanjeKnjige(Program.conn, knjiga.Id);
            }
            else
            {
                Console.WriteLine("Knjiga nije ni bila izdata!");
            }
        }

        //brisanje knjige u biblioteku
        public static void BrisanjeKnjige()
        {
            Knjiga knjiga = PronadjiKnjiguPoNazivuAutoru();

            if (knjiga.Clan == null)
            {
                KnjigaDAL.ObrisiKnjigu(Program.conn, knjiga.Id);
            }
            else
            {
                Console.WriteLine("Knjigu nije moguce obrisati, posto u evidenciji stoji da je izdata!");
            }
        }

    }
}
