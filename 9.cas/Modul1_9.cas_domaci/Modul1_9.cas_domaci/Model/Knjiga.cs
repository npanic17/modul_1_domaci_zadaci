﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_9.cas_domaci.Model
{
    class Knjiga
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Autor { get; set; }
        public string Godina { get; set; }
        public Clan Clan { get; set; }

        public Knjiga()
        {
            Clan = new Clan();
        }

        public Knjiga(int id, string ime, string autor, string godina)
        {
            Id = id;
            Ime = ime;
            Autor = autor;
            Godina = godina;
            
        }

        public Knjiga(int id, string ime, string autor, string godina, Clan clan)
        {
            Id = id;
            Ime = ime;
            Autor = autor;
            Godina = godina;
            Clan = clan;
        }


        public override string ToString()
        {
            if (Clan != null)
            {
                return "[Id=" + Id + "]" + "Ime knjige: " + Ime + " Autor: " + Autor + " Godina: " + Godina + " Knjigu je uzeo: " + Clan.Ime + " " + Clan.Prezime;
            }
            else return "[Id=" + Id + "]" + "Ime knjige: " + Ime + " Autor: " + Autor + " Godina: " + Godina + " Knjiga je slobodna.";


        }
    }
}
