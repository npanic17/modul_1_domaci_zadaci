﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_9.cas_domaci.Model
{
    class Clan
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        const int MAXBRKNJIGA = 3;

        public Clan()
        {
          
        }
        public Clan(int id, string ime, string prezime)
        {
            Id = id;
            Ime = ime;
            Prezime = prezime;
        }
        
        public override string ToString()
        {
            return "[Idclana="+Id+"]"+ " Clan: "+ Ime+ " "+Prezime;
        }
    }
}
