CREATE TABLE Termin09Biblioteka_knjiga (
	Id int primary key identity(1,1),
	Ime nvarchar(50),
	Autor nvarchar(50),
	Godina nvarchar(50),
	ClanId int,
	foreign key(ClanId) references Termin09Biblioteka_clan(Id)

);