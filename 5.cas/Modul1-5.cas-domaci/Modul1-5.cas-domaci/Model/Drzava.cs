﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_5.cas_domaci.Model
{
    public class Drzava
    {
        public string Id { get; set; }
        public string Naziv { get; set; }
        public List<SvetskoPrvenstvo> ListaPrvenstvaDomacin { get; set; }

        public Drzava()
        {
            ListaPrvenstvaDomacin = new List<SvetskoPrvenstvo>();
        }

        public Drzava(string id, string ime)
        {
            Id = id;
            Naziv = ime;
            ListaPrvenstvaDomacin = new List<SvetskoPrvenstvo>();
        }

        public Drzava(string id, string ime, SvetskoPrvenstvo imePrvenstva)
        {
            Id = id;
            Naziv = ime;
            ListaPrvenstvaDomacin = new List<SvetskoPrvenstvo>();
            ListaPrvenstvaDomacin.Add(imePrvenstva);
        }

        //konstruktor koji popunjava podatke na osnovu očitanog teksta iz datoteke drzava
        public Drzava(string tekst)
        {
            string[] tokeni = tekst.Split(',');
            //npr. 		1,Srbija,
            //tokeni 	0		1

            if (tokeni.Length != 2)
            {
                Console.WriteLine("Greska pri ocitavanju drzava " + tekst);
                //izlazak iz aplikacije
                Environment.Exit(0);
            }

            Id = tokeni[0];
            Naziv = tokeni[1];
            ListaPrvenstvaDomacin = new List<SvetskoPrvenstvo>();
        }

        public string IspisAtributaDrzave()
        {
            return "Id: "+Id+" Ime drzave: " + Naziv;
        }

        //ispis za csv
        public string IspisZaCsvFormat()
        {
            return Id + "," + Naziv;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("Id: " + Id + " Ime drzave: " + Naziv);
           
            if (ListaPrvenstvaDomacin.Count>0)
            {
                sb.AppendLine("\n"+"Domacin Svetskog prvenstva je bila:");

                foreach (SvetskoPrvenstvo el in ListaPrvenstvaDomacin)
                {
                    sb.AppendLine("Ime prvenstva: " + el.Naziv + ". Godina odrzavanja: " + el.GodinaOdrzavanja);
                    //if (el.Domacin != null) sb.AppendLine(" Domacin: " + el.Domacin);
                }
            }
            return sb.ToString();
        }
    }
}
