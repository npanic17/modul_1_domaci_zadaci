﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_5.cas_domaci.Model
{
    public class SvetskoPrvenstvo
    {
        public string Id { get; set; }
        public string Naziv { get; set; }
        public string GodinaOdrzavanja { get; set; }
        public Drzava Domacin { get; set; }

        public SvetskoPrvenstvo()
        {
            Drzava Domacin = new Drzava();
        }

        public SvetskoPrvenstvo(string id, string ime, string godina)
        {
            Id = id;
            Naziv = ime;
            GodinaOdrzavanja = godina;
            Drzava Domacin = new Drzava();
        }

        public SvetskoPrvenstvo(string id, string ime, string godina, Drzava drzava): this()
        {
            Id = id;
            Naziv = ime;
            GodinaOdrzavanja = godina;
            Domacin = drzava;
        }

        //konstruktor koji popunjava podatke na osnovu očitanog teksta iz datoteke svetkaprvenstva
        public SvetskoPrvenstvo(string tekst)
        {
            string[] tokeni = tekst.Split(',');
            //npr. 		1,WC Italy, 1990
            //tokeni 	0		1     2

            if (tokeni.Length != 3)
            {
                Console.WriteLine("Greska pri ocitavanju svetsko prvenstvo " + tekst);
                //izlazak iz aplikacije
                Environment.Exit(0);
            }

            Id = tokeni[0];
            Naziv = tokeni[1];
            GodinaOdrzavanja= tokeni[2];
        }
        
        public string IspisAtributaSvetskoPrvenstvo()
        {
            return "Id: " + Id + "Naziv svetskog prvenstva: " + Naziv+" Godina: "+GodinaOdrzavanja;
        }

        public string IspisZaCsvFormat()
        {
            return Id + "," + Naziv + "," +GodinaOdrzavanja;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Naziv svetskog prvenstva: " + Naziv + " Godina odrzavanja: " + GodinaOdrzavanja);
            if (Domacin != null)
            {
                sb.Append(" Domacin: " + this.Domacin.Naziv);
            }
            return sb.ToString();
        }
    }
}
