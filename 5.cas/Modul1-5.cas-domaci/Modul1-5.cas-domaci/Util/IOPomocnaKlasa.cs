﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_5.cas_domaci.Util
{
    class IOPomocnaKlasa
    {
        //citaj int32 broj
        public static int CitajCeoBroj()
        {
            int broj;
            Console.Write("Opcija:");
            while (Int32.TryParse(Console.ReadLine(), out broj) == false)
            {
                Console.WriteLine("niste uneli korektnu vrednost.Unesite ponovo");
            }
            return broj;
        }

        //citanje promenljive string
        public static string OcitajTekst()
        {
            string tekst = "";
            while (tekst == null || tekst.Equals("")) //stringovi se mogu porediti i sa preklopljenim operatorom ==
            {
                tekst = Console.ReadLine();
            }
            return tekst;
        }

        //citanje promenljive char
        public static char OcitajKarakter()
        {
            char c;
            while (Char.TryParse(Console.ReadLine(), out c) == false)
            {
                Console.Write("GRESKA - Pogresno unsesena vrednost, pokusajte ponovo: ");
            }
            return c;
        }


        //potvrda
        public static bool Potvrda(string s)
        {
            Console.WriteLine("Da li zelite " + s + " [d/n]");
            char c = ' ';
            while (!(c == 'd' || c == 'n'))
            {
                c = Char.ToLower(OcitajKarakter());
                if (!(c == 'd' || c == 'n'))
                {
                    Console.WriteLine("opcije su d i n");
                }
            }
            return c == 'd' ? true : false;
        }
    }
}
