﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modul1_5.cas_domaci.Model;
using Modul1_5.cas_domaci.Util;
using Modul1_5.cas_domaci.UI;

namespace Modul1_5.cas_domaci.UI
{
     class DrzavaSvetskoPrvenstvoUI
    {
        //brisanje sv prvenstva iz liste sv.prvenstva koju svaka drzava ima kao field
        public static void UkloniSvPrvSaListePrvenDrzave(Drzava drz)
        {
            Console.WriteLine("lista sv. prvenstava za ovu drzavu");
            foreach (SvetskoPrvenstvo el in drz.ListaPrvenstvaDomacin)
                Console.WriteLine(el.IspisAtributaSvetskoPrvenstvo());
            Console.WriteLine("unesite id sv prvenstva koji zelite da uklonite za ovu drzavu");
            string idprv = IOPomocnaKlasa.OcitajTekst();
            var itemToRemove = drz.ListaPrvenstvaDomacin.Single(r => r.Id == idprv);
            drz.ListaPrvenstvaDomacin.Remove(itemToRemove);
            SvetskoPrvenstvoUI.UkloniSvPrvenstvo(idprv);
        }
        //ucitavanje drzavasvetskoprvenstvo.csv fajla
        public static void UcitajCsvDrzavaSvetskoPrvenstvoUI(string s)
        {
            if (File.Exists(s))
            {
                using (StreamReader sr = File.OpenText(s))
                {
                    //Drzava drz;
                    string tekst = "";
                    while ((tekst = sr.ReadLine()) != null)
                    {
                        string[] tokeni = tekst.Split(',');
                        //npr. 		1,1
                        //tokeni 	0		1

                        if (tokeni.Length != 2)
                        {
                            Console.WriteLine("Greska pri ocitavanju drzavasvetskoprvenstvo " + tekst);
                            //izlazak iz aplikacije
                            Environment.Exit(0);
                        }

                        Drzava drz = DrzavaUI.PronadjiDrzavuPoId(tokeni[0]);
                        SvetskoPrvenstvo pr = SvetskoPrvenstvoUI.PronadjiSvetskoPrvenstvoPoId(tokeni[1]);
                        SvetskoPrvenstvoUI.ListaSvetskihPrvenstva[pr.Id].Domacin = drz;
                        DrzavaUI.ListaDrzava[drz.Id].ListaPrvenstvaDomacin.Add(pr);



                        //drz = new Drzava(linija);
                        //ListaDrzava.Add(drz.Id, drz);
                    }
                }
            }

        }

        //cuvanje u drzavasvetskoprvenstvo.csv fajl
        public static void SacuvajCsvDrzavaSvPrvenstvoUI(string s)
        {
            if (File.Exists(s))
            {
                using (StreamWriter sw = new StreamWriter(s))
                {
                    foreach (KeyValuePair<string, SvetskoPrvenstvo> kvp in SvetskoPrvenstvoUI.ListaSvetskihPrvenstva)
                    {
                        
                        sw.WriteLine(kvp.Value.Domacin.Id+","+ kvp.Value.Id);
                    }
                }
            }

        }


    }
}
