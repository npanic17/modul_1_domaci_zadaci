﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modul1_5.cas_domaci.Util;
using Modul1_5.cas_domaci.Model;

namespace Modul1_5.cas_domaci.UI
{
    static class DrzavaUI
    {
        public static Dictionary<string, Drzava> ListaDrzava { get; set; }

        static DrzavaUI()
        {
            ListaDrzava = new Dictionary<string, Drzava>();
        }

        //ispis opcija DrzavaUI
        public static void IspisDrzavaUIOpcija()
        {
            Console.WriteLine("Evidencija drzava - Osnovne opcije:");
            Console.WriteLine("\tOpcija broj 1 - Ispis svih drzava");
            Console.WriteLine("\tOpcija broj 2 - Unos drzava");
            Console.WriteLine("\tOpcija broj 3 - Izmena drzava");
            Console.WriteLine("\tOpcija broj 4 - Sortiranje drzava po nazivu i ispis");
            Console.WriteLine("\tOpcija broj 5 - Ispis drzava za uneti opseg godina od-do");
            Console.WriteLine("\t\t ...");
            Console.WriteLine("\tOpcija broj 0 - POVRATAK NA GLAVNI MENI");
        }

        //MENI DrzavaUI
        public static void MeniDrzavaUI()
        {
            int odluka = -1;

            while (odluka != 0)
            {

                IspisDrzavaUIOpcija();
                odluka = IOPomocnaKlasa.CitajCeoBroj();
                Console.Clear();
                switch (odluka)
                {
                    case 0:
                        // Console.WriteLine("Izlaz");
                        break;
                    case 1:
                        DrzavaUI.IspisSvihDrzava();
                        break;
                    case 2:
                        DrzavaUI.UnosNoveDrzave();
                        break;
                    case 3:
                        DrzavaUI.IzmenaDrzave();
                        break;
                    default:
                        Console.WriteLine("Nepostojeca komanda!\n\n");
                        break;
                }

            }
        }

        //ispis svih drzava iz liste drzava
        public static void IspisSvihDrzava()
        {
            foreach (KeyValuePair<string, Drzava> kvp in ListaDrzava)
            {
                Console.WriteLine(kvp.Value);
            }
        }

        //unos nove drzave u liste drzava
        public static void UnosNoveDrzave()
        {
            Console.WriteLine("Unesite Id drzave");
            string s = IOPomocnaKlasa.OcitajTekst();
            while (PronadjiDrzPoId(s) != null)
            {
                Console.WriteLine("Drzava sa ovim Id vec postoji!");
                s = IOPomocnaKlasa.OcitajTekst();
            }
            Console.WriteLine("Unesite naziv drzave");
            string ss = IOPomocnaKlasa.OcitajTekst();
            Drzava drz = new Drzava(s, ss);
            ListaDrzava.Add(drz.Id, drz);

        }

        //unos nove drzave u liste drzava sa zadatim id koji je unique
        public static Drzava UnosNoveDrzavesaId(string id)
        {
            while (PronadjiDrzPoId(id) != null)
            {
                Console.WriteLine("Drzava sa ovim Id vec postoji!");
                id = IOPomocnaKlasa.OcitajTekst();
            }
            Console.WriteLine("Unesite naziv drzave");
            string ss = IOPomocnaKlasa.OcitajTekst();
            Drzava drz = new Drzava(id, ss);
            ListaDrzava.Add(drz.Id, drz);
            return drz;
        }

        //Izmena drzave u liste drzava
        public static void IzmenaDrzave()
        {
            Console.WriteLine("Unesite Id drzave");
            string id = IOPomocnaKlasa.OcitajTekst();
            while (PronadjiDrzPoId(id) == null)
            {
                Console.WriteLine("Drzava sa ovim Id ne postoji!");
                id = IOPomocnaKlasa.OcitajTekst();
            }
            Console.WriteLine("Unesite drugi naziv drzave");
            string ss = IOPomocnaKlasa.OcitajTekst();
            ListaDrzava[id].Naziv = ss;
            if (ListaDrzava[id].ListaPrvenstvaDomacin != null)
            {
                if (IOPomocnaKlasa.Potvrda("Da li zelite da obrisete sv prvenstvo za ovu drzavu?Ovom akcijom se brise i sv prvenstvo iz recnika sv.pr"))
                {
                    DrzavaSvetskoPrvenstvoUI.UkloniSvPrvSaListePrvenDrzave(ListaDrzava[id]);   
                }
                if (IOPomocnaKlasa.Potvrda("Da li zelite da dodate sv prvenstvo za ovu drzavu?"))
                {
                    SvetskoPrvenstvoUI.UnosNovogSvPrvenstva();
                }
            }
        }

        // pronadji drzavu
        public static Drzava PronadjiDrzavu()
        {
            Drzava retVal = null;
            Console.WriteLine("Unesi id drzave:");
            string id = IOPomocnaKlasa.OcitajTekst();
            try
            {
                retVal = PronadjiDrzavuPoId(id);
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Predmet sa id-om " + id + " ne postoji u evidenciji");
            }

            return retVal;
        }

        //pronadji drzavu po Id
        public static Drzava PronadjiDrzPoId(String stIndex)
        {
            Drzava retVal = null;
            foreach (string drz in ListaDrzava.Keys)
            {
                if (drz == stIndex)
                {
                    retVal = ListaDrzava[drz];
                    break;
                }
            }
            return retVal;
        }
        //pronadji drzavu po Id
        public static Drzava PronadjiDrzavuPoId(string id)
        {
            try
            {
                return ListaDrzava[id];
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Ne postoji vrednost u rečniku RecnikDrzava za dati ključ!");
                throw;
            }
        }

        //ucitavanje drzava.csv fajla
        public static void UcitajCsvDrzavaUI(string s)
        {
            if (File.Exists(s))
            {
                using (StreamReader sr = File.OpenText(s))
                {
                    Drzava drz;
                    string linija = "";
                    while ((linija = sr.ReadLine()) != null)
                    {
                        drz = new Drzava(linija);
                        ListaDrzava.Add(drz.Id, drz);
                    }
                }
            }

        }

        //cuvanje u drzava.csv fajl
        public static void SacuvajCsvDrzavaUI(string s)
        {
            if (File.Exists(s))
            {
                using (StreamWriter sw = new StreamWriter(s))
                {
                    foreach (KeyValuePair<string, Drzava> kvp in ListaDrzava)
                    {
                        sw.WriteLine(kvp.Value.IspisZaCsvFormat());
                    }
                }
            }

        }

    }
}
