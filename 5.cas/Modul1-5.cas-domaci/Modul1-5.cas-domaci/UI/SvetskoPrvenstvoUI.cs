﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modul1_5.cas_domaci.Util;
using Modul1_5.cas_domaci.Model;

namespace Modul1_5.cas_domaci.UI
{
    class SvetskoPrvenstvoUI
    {
        public static Dictionary<string, SvetskoPrvenstvo> ListaSvetskihPrvenstva { get; set; }

        static SvetskoPrvenstvoUI()
        {
            ListaSvetskihPrvenstva = new Dictionary<string, SvetskoPrvenstvo>();
        }

        //ispis opcija SvPrvenstvaUI
        public static void IspisSvPrvenstvaUIOpcija()
        {
            Console.WriteLine("Evidencija drzava - Osnovne opcije:");
            Console.WriteLine("\tOpcija broj 1 - Ispis svih sv. prvenstva");
            Console.WriteLine("\tOpcija broj 2 - Unos sv. prvenstva");
            Console.WriteLine("\tOpcija broj 3 - Izmena sv. prvenstva");
            Console.WriteLine("\tOpcija broj 4 - Sortiranje sv. prvenstva po nazivu i godini odrzavanja");
            Console.WriteLine("\t\t ...");
            Console.WriteLine("\tOpcija broj 0 - POVRATAK NA GLAVNI MENI");
        }

        //ispis svih sv. prvenstva iz liste sv. prvenstva
        public static void IspisSvihSvPrvenstva()
        {
            foreach (KeyValuePair<string, SvetskoPrvenstvo> kvp in ListaSvetskihPrvenstva)
            {
                Console.WriteLine(kvp.Value);
            }
        }

        //MENI SvPrvenstvoUI
        public static void MeniSvPrvenstvoUI()
        {
            int odluka = -1;

            while (odluka != 0)
            {

                IspisSvPrvenstvaUIOpcija();
                odluka = IOPomocnaKlasa.CitajCeoBroj();
                Console.Clear();
                switch (odluka)
                {
                    case 0:
                        // Console.WriteLine("Izlaz");
                        break;
                    case 1:
                        IspisSvihSvPrvenstva();
                        break;
                    case 2:
                        UnosNovogSvPrvenstva();
                        break;
                    case 3:
                        IzmenaSvPrvenstva();
                        break;
                    default:
                        Console.WriteLine("Nepostojeca komanda!\n\n");
                        break;
                }

            }
        }

        //unos novog sv prvenstva u liste sv prvenstva
        public static void UnosNovogSvPrvenstva()
        {
            Console.WriteLine("Unesite Id svetskog prvenstva");
            string s = IOPomocnaKlasa.OcitajTekst();
            while (PronadjiSvPrvPoId(s) != null)
            {
                Console.WriteLine("Svetsko prvenstvi sa ovim Id vec postoji!");
                s = IOPomocnaKlasa.OcitajTekst();
            }
            Console.WriteLine("Unesite naziv svetskog prvenstva");
            string ss = IOPomocnaKlasa.OcitajTekst();
            Console.WriteLine("Unesite godinu odrzavanja svetskog prvenstva");
            string sss = IOPomocnaKlasa.OcitajTekst();

            Console.WriteLine("Unesite id drzave domacina");
            string IdDrzave = IOPomocnaKlasa.OcitajTekst();
            Drzava drz;
            if ((drz = DrzavaUI.PronadjiDrzPoId(IdDrzave)) != null)
            {
                SvetskoPrvenstvo prv = new SvetskoPrvenstvo(s, ss, sss, drz);
                ListaSvetskihPrvenstva.Add(prv.Id, prv);
            }
            else
            {
                Console.WriteLine("Drzava sa ovim id ne postoji. Unesite novu drzavu:");
                drz = DrzavaUI.UnosNoveDrzavesaId(IdDrzave);
                SvetskoPrvenstvo prv = new SvetskoPrvenstvo(s, ss, sss, drz);
                ListaSvetskihPrvenstva.Add(prv.Id, prv);
            }
        }

        //brisanje svetskog prvenstva iz liste sv prvenstva
        public static void UkloniSvPrvenstvo(string id)
        {
            SvetskoPrvenstvo prv=PronadjiSvetskoPrvenstvoPoId(id);
            if (prv != null)
            {
                ListaSvetskihPrvenstva.Remove(prv.Id);
                Console.WriteLine("Sv prvenstvo obrisano iz recnika");
            }
        }

        //Izmena sv prvenstvo u listi sv prvenstva
        public static void IzmenaSvPrvenstva()
        {
            Console.WriteLine("Unesite Id sv prvenstva");
            string id = IOPomocnaKlasa.OcitajTekst();
            while (PronadjiSvPrvPoId(id) == null)
            {
                Console.WriteLine("Sv. prvenstvo sa ovim Id ne postoji!");
                id = IOPomocnaKlasa.OcitajTekst();
            }
            Console.WriteLine("Unesite drugi naziv sv.prvenstva");
            string ss = IOPomocnaKlasa.OcitajTekst();
            Console.WriteLine("Unesite drugu godinu odrzvanja sv.prvenstva");
            string sss = IOPomocnaKlasa.OcitajTekst();
            ListaSvetskihPrvenstva[id].Naziv = ss;
            ListaSvetskihPrvenstva[id].GodinaOdrzavanja = sss;
            //if (ListaSvetskihPrvenstva[id].Domacin != null)
            //{
            //    if (IOPomocnaKlasa.Potvrda("Da li zelite da izmenite podatke domacina?"))
            //    {
            //        DrzavaSvetskoPrvenstvoUI.UkloniSvPrvSaListePrvenDrzave(ListaDrzava[id]);
            //    }
            //    if (IOPomocnaKlasa.Potvrda("Da li zelite da dodate sv prvenstvo za ovu drzavu?"))
            //    {
            //        SvetskoPrvenstvoUI.UnosNovogSvPrvenstva();
            //    }
            //}
        }


        //pronadji svetskoprvenstvo po Id
        public static SvetskoPrvenstvo PronadjiSvPrvPoId(String stIndex)
        {
            SvetskoPrvenstvo retVal = null;
            foreach (string prv in ListaSvetskihPrvenstva.Keys)
            {
                if (prv == stIndex)
                {
                    retVal = ListaSvetskihPrvenstva[prv];
                    break;
                }
            }
            return retVal;
        }

        //pronadji svetskoprvenstvo po Id
        public static SvetskoPrvenstvo PronadjiSvetskoPrvenstvoPoId(string id)
        {
            try
            {
                return ListaSvetskihPrvenstva[id];
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Ne postoji vrednost u rečniku RecnikSvetskoPrvenstvo za dati ključ!");
                throw;
            }
        }

        //ucitavanje svetskoprvenstvo.csv fajla
        public static void UcitajCsvSvPrvenstvoUI(string s)
        {
            if (File.Exists(s))
            {
                using (StreamReader sr = File.OpenText(s))
                {
                    SvetskoPrvenstvo svpr;
                    string linija = "";
                    while ((linija = sr.ReadLine()) != null)
                    {
                        svpr = new SvetskoPrvenstvo(linija);
                        ListaSvetskihPrvenstva.Add(svpr.Id, svpr);
                    }
                }
            }

        }

        //cuvanje u svetskoprvenstvo.csv fajl
        public static void SacuvajCsvSvPrvenstvoUI(string s)
        {
            if (File.Exists(s))
            {
                using (StreamWriter sw = new StreamWriter(s))
                {
                    foreach (KeyValuePair<string, SvetskoPrvenstvo> kvp in ListaSvetskihPrvenstva)
                    {
                        sw.WriteLine(kvp.Value.IspisZaCsvFormat());
                    }
                }
            }

        }









    }
}
