﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modul1_5.cas_domaci.Model;
using Modul1_5.cas_domaci.Util;


namespace Modul1_5.cas_domaci.UI
{
    static class AplikacijaUI
    {
        private static readonly string DataDir = "data";
        private static readonly string DrzavaDat = "drzave.csv";
        private static readonly string SvetskoPrvenstvoDat = "svetskaprvenstva.csv";
        private static readonly string StudDat = "drzavesvetskaprvenstva.csv";
        private static readonly char sep = Path.DirectorySeparatorChar;
        private static string putanjaDataDirRelease = "data";

        //podesenj putanje gde su smesteni csv fajlovi
        private static string PodesiPutanju()
        {
            string trenutnaPutanja = Directory.GetCurrentDirectory();
            string putanja = "";
            string putanjaProjekta = new DirectoryInfo(trenutnaPutanja).Parent.Parent.FullName;
            return putanja = putanjaProjekta + sep + DataDir + sep;
        }

        //provera postojanja foldera data i potrebnih fajlova
        private static void ProveraCsvIFoldera(string s)
        {

            if (!Directory.Exists(s))
            {
                Console.WriteLine("Putanja nije ispravna ili ne postoji folder" + s);
            }
            else if (!File.Exists(s + DrzavaDat) || !File.Exists(s + SvetskoPrvenstvoDat) || !File.Exists(s + StudDat))
            {
                Console.WriteLine("nedostaju fajlovi");
            }
            else
            {
                return;
            }
            Console.WriteLine("\nPritisnite bilo koji taster...");
            Console.ReadKey(true);
            Environment.Exit(1);
        }



        //ispis opcija AppUI
        public static void IspisAplikacijaUIOpcija()
        {
            Console.WriteLine("Evidencija drzava i svetskih prvenstava - Osnovne opcije:");
            Console.WriteLine("\tOpcija broj 1 - Rad sa drzavama");
            Console.WriteLine("\tOpcija broj 2 - Rad sa svetskim prvenstvima");
            Console.WriteLine("\t\t ...");
            Console.WriteLine("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
        }


        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            string putanjaDataDir = PodesiPutanju();
            ProveraCsvIFoldera(putanjaDataDir);

            DrzavaUI.UcitajCsvDrzavaUI(putanjaDataDir+DrzavaDat);
            SvetskoPrvenstvoUI.UcitajCsvSvPrvenstvoUI(putanjaDataDir+SvetskoPrvenstvoDat);
            DrzavaSvetskoPrvenstvoUI.UcitajCsvDrzavaSvetskoPrvenstvoUI(putanjaDataDir + StudDat);



            //MENI AplikacijaUI
            int odluka = -1;
            while (odluka != 0)
            {
                
                IspisAplikacijaUIOpcija();
                odluka = IOPomocnaKlasa.CitajCeoBroj();
                Console.Clear();
                switch (odluka)
                {
                    case 0:
                        Console.WriteLine("Izlaz iz programa");
                        break;
                    case 1:
                        DrzavaUI.MeniDrzavaUI();
                        break;
                    case 2:
                        SvetskoPrvenstvoUI.MeniSvPrvenstvoUI();
                        break;
                    default:
                        Console.WriteLine("Nepostojeca komanda!\n\n");
                        break;
                }

            }

            //DrzavaUI.ListaDrzava["1"].Naziv = "SCG";
            //SvetskoPrvenstvoUI.ListaSvetskihPrvenstva["1"].GodinaOdrzavanja = "1956";
            //SvetskoPrvenstvoUI.ListaSvetskihPrvenstva["1"].Domacin.Id = "88";


            DrzavaUI.SacuvajCsvDrzavaUI(putanjaDataDir + DrzavaDat);
            SvetskoPrvenstvoUI.SacuvajCsvSvPrvenstvoUI(putanjaDataDir + SvetskoPrvenstvoDat);
            DrzavaSvetskoPrvenstvoUI.SacuvajCsvDrzavaSvPrvenstvoUI(putanjaDataDir + StudDat);

            ////List<Drzava> listaDrzava = new List<Drzava> { new Drzava("Srbija"), new Drzava("Rusija") };
            //Drzava drzava = new Drzava("id=1", "Srbija");
            //drzava.ListaPrvenstvaDomacin.Add("id=2", new SvetskoPrvenstvo("id=2", "WC 2018", "2018"));
            //Console.WriteLine(drzava);

            ////SvetskoPrvenstvo prvenstvo = new SvetskoPrvenstvo("WC 2022", "2022",drzava);
            //// Console.WriteLine(prvenstvo);

            Console.ReadKey();
        }
    }
}
