﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VezbanjeAbstractKlase
{
    class Program
    {
        static void Main(string[] args)
        {
            Circle c = new Circle("krug");
            Hex h = new Hex("heksa");
            List<Shape> lista =new List<Shape> { new Circle(), new Hex() };
            lista.Add(c);
            lista.Add(h);
            foreach (Shape el in lista) el.Draw();

            ThreeDCircle t3d = new ThreeDCircle();
            t3d.Draw();

            Console.ReadKey();

        }
    }
}
