﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VezbanjeAbstractKlase
{
    abstract class Shape
    {
        public string PetName { get; set; }
        
        public Shape(string ime)
        {
            PetName = ime;
        }
        public Shape()
        {

        }

        public abstract void Draw();
        
    }

    class Circle: Shape
    {
        public Circle() { }
        public Circle(string ime): base(ime) { }
        public override void Draw()
        {
            Console.WriteLine("drawing {0} circle", PetName);
        }

    }

    class Hex: Shape
    {
        public Hex() { }
        public Hex(string ime): base(ime) { }
        public override void Draw()
        {
            Console.WriteLine("drawing {0} hex",PetName);
        }
    }

    class ThreeDCircle: Circle
    {
        public new void Draw()
        {
            Console.WriteLine("drawing 3d circle");
        }
    }
}
