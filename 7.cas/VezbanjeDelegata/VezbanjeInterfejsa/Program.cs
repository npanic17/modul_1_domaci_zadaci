﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VezbanjeInterfejsa
{
    class Program
    {
        static void Main(string[] args)
        {
            Hex objekat = new Hex();
            Circle c = new Circle();
            //  Console.WriteLine("{0}", objekat.Points);

            IPointy ip = c as IPointy;

            if (ip != null) Console.WriteLine("{0}", ip.Points);
            else Console.WriteLine("not pointy");

            Shape[] list = { new Hex(), new Circle(), new Triangle(), new ThreeDCircle() };
            foreach (var el in list)
            {
                el.Draw();
                if (el is IPointy)
                {
                    Console.WriteLine("points: {0}", ((IPointy)el).Points);
                }
                else
                {
                    Console.WriteLine("no points!");
                }

            }
            Console.WriteLine();
            foreach (var el in list)
            {
                if (el is IDraw3D)
                {
                    DrawIn3D((IDraw3D)el);
                }
               

            }

            Console.ReadKey();


        }

        private static void CloneMe(ICloneable c)
        {
            // Clone whatever we get and print out the name.
            object theClone = c.Clone();
            Console.WriteLine("Your clone is a: {0}",
            theClone.GetType().Name);
        }

        public static void DrawIn3D(IDraw3D itf3d)
        {
            Console.WriteLine("drawing idraw3d compatible type");
            itf3d.Draw3D();
        }
    }
}
