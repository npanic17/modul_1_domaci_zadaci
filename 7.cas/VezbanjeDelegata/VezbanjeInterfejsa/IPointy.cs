﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VezbanjeInterfejsa
{
    // This interface defines the behavior of "having points."
    public interface IPointy
    {
        byte Points { get; }

    }
}
