﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VezbanjeInterfejsa
{
    abstract class Shape
    {
        public string PetName { get; set; }
        
        public Shape(string ime)
        {
            PetName = ime;
        }
        public Shape()
        {

        }

        public abstract void Draw();
        
    }

    class Circle: Shape
    {
        public Circle() { }
        public Circle(string ime): base(ime) { }
        public override void Draw()
        {
            Console.WriteLine("drawing {0} circle", PetName);
        }

    }

    class Hex: Shape,IPointy,IDraw3D
    {
        public Hex() { }
        public Hex(string ime): base(ime) { }
        public override void Draw()
        {
            Console.WriteLine("drawing {0} hex",PetName);
        }

        public byte Points { get { return 6; } }

        public void Draw3D()
        {
            Console.WriteLine("drawing hex in 3D");
        }
    }

    class ThreeDCircle: Circle, IDraw3D
    {
        public new void Draw()
        {
            Console.WriteLine("drawing 3d circle");
        }

        public void Draw3D()
        {
            Console.WriteLine("drawing circle in 3D");
        }
    }

    class Triangle: Shape, IPointy
    {
        public Triangle() { }
        public Triangle(string ime): base(ime) { }
        public override void Draw()
        {
            Console.WriteLine("drawing {0} triangle", PetName);
        }
        public byte Points { get { return 3; }  }
    }
}
