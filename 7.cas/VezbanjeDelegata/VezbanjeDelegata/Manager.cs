﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VezbanjeDelegata
{
    class Manager: Employee
    {
        public int StockOptions { get; set; }

        public Manager() { }
        public Manager(string name, int age, int id, float pay, string ssn, int opt): base(name, age, id, pay, ssn)
        {
            StockOptions = opt;
        }

        public override void GiveBonus(float amount)
        {
            base.GiveBonus(amount);
            Random r = new Random();
            StockOptions += r.Next(500);
        }

        public override void DisplayStats()
        {
            base.DisplayStats();
            Console.WriteLine("Broj akcija {0}",StockOptions);
        }
    }
}
