﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VezbanjeDelegata
{
    class SalesPerson : Employee
    {
        public int SalesNumber { get; set; }

        public SalesPerson() { }
        public SalesPerson(string name, int age, int id, float pay, string ssn, int opt) : base(name, age, id, pay, ssn)
        {
            SalesNumber = opt;
        }

        public override void GiveBonus(float amount)
        {
            int salesBonus = 0;
            if (SalesNumber >= 0 && SalesNumber <= 100)
                salesBonus = 10;
            else
            {
                if (SalesNumber >= 101 && SalesNumber <= 200)
                    salesBonus = 15;
                else
                    salesBonus = 20;
            }
            base.GiveBonus(amount*salesBonus);
        }

        public sealed override void DisplayStats()
        {
            base.DisplayStats();
            Console.WriteLine("Broj prodatih artikala {0}",SalesNumber);
        }

        
    }
}
