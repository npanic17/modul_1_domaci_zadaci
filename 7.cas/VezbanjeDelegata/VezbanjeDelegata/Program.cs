﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VezbanjeDelegata
{
    public delegate void HelloFuncDelegate(string x);

    class Program
    {


        static void Main(string[] args)
        {

            SalesPerson ivan = new SalesPerson();
            ivan.GiveBonus(500);
            ivan.DisplayStats();

            Manager nemanja = new Manager();
            nemanja.GiveBonus(100);
            nemanja.DisplayStats();

            Console.ReadKey();
        }




    }
};
