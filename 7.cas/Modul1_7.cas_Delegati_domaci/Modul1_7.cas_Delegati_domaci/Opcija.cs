﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_7.cas_Delegati_domaci
{
    public delegate void Del();

    class Opcija
    {
        public string OpisOpcije { get; set; }
        public Del Del1 { get; set; }

        public Opcija(string opisOpcije, Del del1)
        {
            OpisOpcije = opisOpcije;
            this.Del1 = del1;
        }

       


    }
}
