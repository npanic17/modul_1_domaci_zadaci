﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_7.cas_Delegati_domaci
{
    class Meni
    {

        List<Opcija> ListaOpcija;

        public Meni()
        {
            ListaOpcija = new List<Opcija>();
        }

        public void DodajOpciju(Opcija Op)
        {
            ListaOpcija.Add(Op);
        }

        // Ispis jednostavnog menija, upit za opciju, i izvrsavanje odabrane
        // opcije. Bitno je primetiti da prilikom kompajliranja se ne zna
        // koje tacno metode ce se izvrsiti kada se pozovu promenljive
        // delegati.
        public void Pokreni()
        {

            int odluka = -1;
            while (odluka != 0)
            {
                Console.WriteLine("Opcije:");
                for (int i = 0; i < ListaOpcija.Count; i++)
                {
                    Console.Write("{0} - ", i + 1);
                    Console.WriteLine(ListaOpcija[i].OpisOpcije);
                }
                Console.WriteLine("----------");
                Console.WriteLine("0 - Nazad:");

                odluka = IO.CitajBroj();
                switch (odluka)
                {
                    case 0:
                        break;
                    case 1:
                        ListaOpcija[0].Del1();
                        break;
                    case 2:
                        ListaOpcija[1].Del1();
                        break;
                    default:
                        Console.WriteLine("Unknown option!");
                        break;
                }
            }


        }
    }
}
