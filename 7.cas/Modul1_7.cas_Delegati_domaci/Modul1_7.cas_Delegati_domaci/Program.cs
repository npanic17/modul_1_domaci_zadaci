﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_7.cas_Delegati_domaci
{
    class Program
    {
        static void PozdravPera()
        {
            Console.WriteLine("Pera pozdravlja");
        }

        static void PozdravZika()
        {
            Console.WriteLine("Zika pozdravlja");
        }

        static void Main(string[] args)
        {
            Meni m = new Meni();
            m.DodajOpciju(new Opcija("pera sve najbolje", PozdravPera));
            m.DodajOpciju(new Opcija("Zika sve najbolje", PozdravZika));

            m.Pokreni();
            

        }
    }
}
