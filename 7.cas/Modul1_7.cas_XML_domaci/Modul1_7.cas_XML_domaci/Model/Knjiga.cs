﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Modul1_7.cas_XML_domaci.Model
{

    [XmlInclude(typeof(Roman))]
    [XmlInclude(typeof(Recnik))]
    [Serializable]
    public class Knjiga
    {
        
        
        public string Naziv { get; set; }
        public string BrojStrana { get; set; }

        public Knjiga()
        {

        }
        public Knjiga(string naziv, string brojStrana)
        {
            Naziv = naziv;
            BrojStrana = brojStrana;
        }

        public override string ToString()
        {
            return "Naziv: " + Naziv + " Br. strana: " + BrojStrana;
        }
    }
}
