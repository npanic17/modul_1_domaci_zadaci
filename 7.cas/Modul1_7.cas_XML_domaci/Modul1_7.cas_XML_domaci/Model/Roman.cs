﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Modul1_7.cas_XML_domaci.Model
{
    [Serializable]
    public class Roman: Knjiga
    {
       
        public string Pisac { get; set; }

        public Roman(string Naziv, string BrStrana, string pisac) : base(Naziv, BrStrana)
        {
            Pisac = pisac;
        }
        public Roman(): base()
        {
           
        }

        public override string ToString()
        {
            return base.ToString()+" Pisac: "+Pisac;
        }
    }
}
