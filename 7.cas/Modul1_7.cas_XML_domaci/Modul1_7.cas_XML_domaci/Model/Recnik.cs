﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Modul1_7.cas_XML_domaci.Model
{
    [Serializable]
    public class Recnik : Knjiga
    {

        public string BrReci { get; set; }
        public string Izvorni { get; set; }
        public string Ciljani { get; set; }

        public Recnik()
        {

        }
        public Recnik(string naziv, string brstrana, string brReci, string izvorni, string ciljani) : base(naziv, brstrana)
        {
            BrReci = brReci;
            Izvorni = izvorni;
            Ciljani = ciljani;
        }

        public override string ToString()
        {
            return base.ToString() + " Br. reci: " + BrReci + " Izvorni: " + Izvorni + " Ciljani: " + Ciljani;
        }
    }
}
