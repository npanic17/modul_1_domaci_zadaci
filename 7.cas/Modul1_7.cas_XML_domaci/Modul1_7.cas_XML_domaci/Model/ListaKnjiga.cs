﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_7.cas_XML_domaci.Model
{
    [Serializable]
  public  class ListaKnjiga
    {

        public List<Knjiga> Lista { get; set; }


        public ListaKnjiga(List<Knjiga> lista)
        {
            Lista = lista;
        }

        public ListaKnjiga()
        {
            Lista = new List<Knjiga>();
        }

        
    }
}
