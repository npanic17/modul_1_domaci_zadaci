﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modul1_7.cas_XML_domaci.Model;
using System.Xml.Serialization;
using System.IO;

namespace Modul1_7.cas_XML_domaci
{
    class Program
    {
        static void Main(string[] args)
        {
            ListaKnjiga ListaKnjiga = new ListaKnjiga();
            Roman r1 = new Roman("Na drini cuprija", "150", "Ivo Andric");
            Recnik rec1 = new Recnik("srpsko-eng", "1000", "550", "srp", "eng");
            ListaKnjiga.Lista.Add(r1);
            ListaKnjiga.Lista.Add(rec1);

            XmlSerializer ser = new XmlSerializer(typeof(ListaKnjiga));
            using (StreamWriter wr = new StreamWriter(@"E:\Listaknjiga.xml", false, Encoding.UTF8))
            {
                ser.Serialize(wr, ListaKnjiga);
            }
            
        }
    }
}
