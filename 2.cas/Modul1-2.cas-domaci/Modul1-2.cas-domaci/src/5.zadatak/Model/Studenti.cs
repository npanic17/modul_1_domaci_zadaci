﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_2.cas_domaci.src._5.zadatak
{
    public class Studenti
    {
        private int[] StudentId = new int[7];
        private string[] smer = new string[7], Ime = new string[7], Prezime = new string[7], Grad = new string[7], index = new string[7];

        public void Ucitaj(string s)
        {
            string[] ss = s.Split('\n');
            string[] sss = null;
            for (int i = 0; i < ss.Length; i++)
            {
                sss = ss[i].Split(',');
                StudentId[i] = Int32.Parse(sss[0]);
                string[] ssss = sss[1].Split();
                smer[i] = ssss[0];
                index[i] = ssss[1];
                Prezime[i] = sss[2];
                Ime[i] = sss[3];
                Grad[i] = sss[4];
            }
        }

        public void Ispis()
        {
            for (int i = 0; i < StudentId.Length; i++)
            {
                Console.WriteLine("StudentId:" + StudentId[i]);
                Console.WriteLine("Smer:" + smer[i]);
                Console.WriteLine("Index:" + index[i]);
                Console.WriteLine("Ime:" + Ime[i]);
                Console.WriteLine("Prezime:" + Prezime[i]);
                Console.WriteLine("Grad:" + Grad[i]);
                Console.WriteLine();
            }
        }

        public void IspisPoStudentId(int Id)
        {
            for (int i = 0; i < StudentId.Length; i++)
            {
                if (i == (Id - 1))
                {
                    Console.WriteLine("StudentId:" + StudentId[i]);
                    Console.WriteLine("Smer:" + smer[i]);
                    Console.WriteLine("Index:" + index[i]);
                    Console.WriteLine("Ime:" + Ime[i]);
                    Console.WriteLine("Prezime:" + Prezime[i]);
                    Console.WriteLine("Grad:" + Grad[i]);
                    Console.WriteLine();
                }
            }
        }

        public void IspisPoSmeru(string s)
        {
            for (int i = 0; i < StudentId.Length; i++)
            {
                if (smer[i].Equals(s))
                {
                    Console.WriteLine("StudentId:" + StudentId[i]);
                    Console.WriteLine("Smer:" + smer[i]);
                    Console.WriteLine("Index:" + index[i]);
                    Console.WriteLine("Ime:" + Ime[i]);
                    Console.WriteLine("Prezime:" + Prezime[i]);
                    Console.WriteLine("Grad:" + Grad[i]);
                    Console.WriteLine();
                }
            }
        }

    }
}
