﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_2.cas_domaci.src._5.zadatak
{
    class Predmeti
    {
        private int[] PredmetId = new int[4];
        private string[] Ime = new string[5];

        public void Ucitaj(string s)
        {
            string[] ss = s.Split('\n');
            string[] sss = null;
            for (int i = 0; i < ss.Length; i++)
            {
                sss = ss[i].Split(',');
                PredmetId[i] = Int32.Parse(sss[0]);
                Ime[i] = sss[1];
            }
        }

        public void Ispis()
        {
            for (int i = 0; i < PredmetId.Length; i++)
            {
                Console.WriteLine("PredmetId:" + PredmetId[i]);
                Console.WriteLine("Ime:" + Ime[i]);
                Console.WriteLine();
            }
        }

        public void IspisPoPredmetId(int Id)
        {
            for (int i = 0; i < PredmetId.Length; i++)
            {
                if (i == (Id - 1))
                {
                    Console.WriteLine("PredmetId:" + PredmetId[i]);
                    Console.WriteLine("Ime:" + Ime[i]);
                    Console.WriteLine();
                }
            }
        }

    }
}
