﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modul1_2.cas_domaci.src._3.zadatak;

namespace Modul1_2.cas_domaci.src._3.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            FiskalniRacun primer = new FiskalniRacun();
            primer.NazivProdavnice = args[0];
            primer.Stampa();
            Console.ReadKey();
        }
    }
}
