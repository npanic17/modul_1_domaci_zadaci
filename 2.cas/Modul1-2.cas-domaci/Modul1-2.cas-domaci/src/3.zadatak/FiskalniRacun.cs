﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_2.cas_domaci.src._3.zadatak
{
    public class FiskalniRacun
    {
        public string NazivProdavnice;
        private string[] NazivArtikla = { "hleb", "kafa", "Meso", "Mleko" };
        private double[] Kolicina = { 2, 1, 0.85, 1.5 };
        private string[] JedinicaKolicine = { "komad(a)", "komad(a)", "Kg", "Litar(a)" };
        private double[] CenaPoKomadu = { 50, 120, 450, 60 };
        private double[] NaAkciji = { 1, 1, 1, 0.9 };
        private double[] PdvPoArtiklu = { 1, 1.2, 1.1, 1.2 };
        double UkupnaCena = 0, UkupnaCenaSaPDV = 0;

        public double RacunBezPdv()
        {
            double suma = 0;
            for (int i = 0; i < Kolicina.Length; i++)
            {
                suma += Kolicina[i] * CenaPoKomadu[i] * NaAkciji[i];
            }
            return suma;
        }

        public double RacunSaPdv()
        {

            double suma = 0;
            for (int i = 0; i < Kolicina.Length; i++)
            {
                suma += Kolicina[i] * CenaPoKomadu[i] * NaAkciji[i] * PdvPoArtiklu[i];
            }
            return suma;
        }

        public void Stampa()
        {
            Console.WriteLine("Fiskalni racun:");
            for (int i = 0; i < Kolicina.Length; i++)
            {
                Console.WriteLine(NazivArtikla[i]);
                Console.WriteLine(Kolicina[i] + JedinicaKolicine[i] + " X       " + CenaPoKomadu[i] + "din");
                Console.WriteLine("                     " + Kolicina[i] * CenaPoKomadu[i]);
            }
            Console.WriteLine("----------");
            Console.WriteLine("Za uplatu bez PDV: " + RacunBezPdv());
            Console.WriteLine("Za uplatu sa PDV: " + RacunSaPdv());

        }


    }
}
