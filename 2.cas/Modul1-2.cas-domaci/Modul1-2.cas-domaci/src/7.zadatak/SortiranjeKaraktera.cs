﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_2.cas_domaci.src._7.zadatak
{
    class SortiranjeKaraktera
    {

       public static char[] SortUp(string niz)
        {
            char[] arr;
            arr = niz.ToCharArray();
            int n = arr.Length;
            for (int i = 0; i < n - 1; i++)
            {
                int min = i;

                for (int j = i + 1; j < n; j++)
                {
                    if (arr[min] < arr[j]) min = j;
                }

                char temp = arr[i]; arr[i] = arr[min]; arr[min] = temp;
            }
            return arr;
        }

        public static char[] SortDown(string niz)
        {
            char[] arr;
            arr = niz.ToCharArray();
            int n = arr.Length;
            for (int i = 0; i < n - 1; i++)
            {
                int min = i;

                for (int j = i + 1; j < n; j++)
                {
                    if (arr[min] > arr[j]) min = j;
                }

                char temp = arr[i]; arr[i] = arr[min]; arr[min] = temp;
            }
            return arr;
        }

        static void Main(string[] args)
        {
            string niz = "RWRzBRAaBWARwBZ";

            char[] arr=SortUp(niz);
            foreach (var c in arr)
                Console.Write(c);

            Console.WriteLine();

            arr = SortDown(niz);
            foreach (var c in arr)
                Console.Write(c);


            Console.ReadKey();

        }

    }
}
