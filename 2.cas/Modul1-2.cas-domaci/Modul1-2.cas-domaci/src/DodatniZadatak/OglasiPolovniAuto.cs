﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_2.cas_domaci.src.DodatniZadatak
{
    public class OglasiPolovniAuto
    {
        #region uradjeno
        public static void IspisSvihOglasa(ArrayList sifra, ArrayList naslov, ArrayList cena, ArrayList godiste, ArrayList[] oprema)
        {
            Console.WriteLine("Ispis svih oglasa:");
            for (int i = 0; i < sifra.Count; i++)
            {
                Console.WriteLine("Sifra " + sifra[i] + " Naslov oglasa: " + naslov[i] + " Cena auta: " + cena[i] + " Godiste: " + godiste[i]);
                Console.Write("Oprema:");
                foreach (ArrayList s in oprema[i])
                {
                    foreach (var ss in s) Console.Write(" " + ss);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }


        public static void IspisSvihOglasaPoGodistu(ArrayList sifra, ArrayList naslov, ArrayList cena, ArrayList godiste, string S)
        {
            Console.WriteLine("Ispis svih oglasa sa godistem {0}:", S);

            for (int i = 0; i < godiste.Count; i++)
            {
                if ((string)godiste[i] == S)
                {
                    Console.WriteLine("Sifra: " + sifra[i] + " Naslov oglasa: " + naslov[i] + " Cena auta: " + cena[i]);
                }
            }
            Console.WriteLine();
        }

        public static void IspisDetaljaJednogOglasa(ArrayList sifra, ArrayList naslov, ArrayList cena, ArrayList godiste, ArrayList[] oprema, string S)
        {
            int a = sifra.IndexOf(S);
            Console.WriteLine("Ispis izabranog oglasa:");
            Console.WriteLine("Sifra " + sifra[a] + " Naslov oglasa: " + naslov[a] + " Cena auta: " + cena[a] + " Godiste: " + godiste[a]);
            Console.Write("Oprema:");
            foreach (ArrayList s in oprema[a])
            {
                foreach (var ss in s) Console.Write(" " + ss);
            }
            Console.WriteLine();
        }

        #endregion

        // izmena 1 oglasa izabranog po sifri
        public static void IzmenaOglasaOsnovno(ArrayList sifra, ArrayList naslov, ArrayList cena, ArrayList godiste, string S)
        {
            int i = sifra.IndexOf(S);
            Console.WriteLine("Unesi novi naslov oglasa:");
            naslov[i] = Console.ReadLine();
            Console.WriteLine("Unesi novu cenu auta:");
            cena[i] = Console.ReadLine();
            Console.WriteLine("Unesi novo godiste auta:");
            godiste[i] = Console.ReadLine();
        }

        public static void IzmenaOglasaOpreme(ArrayList sifra,ArrayList[] oprema, string S)
        {
            int i = sifra.IndexOf(S);

            Console.Write("Oprema:");
            foreach (ArrayList s in oprema[i]) 
            {
                foreach (var ss in s) Console.Write(" " + ss);
            }
            Console.WriteLine();
            //Console.WriteLine("Koju navedenu opremu zelite da zamenite:?");
            //string primi = Console.ReadLine(); int pom = oprema[i].IndexOf(primi);
            Console.WriteLine("Unesite naziv nove opreme koja ce biti zamenjena sa 1. u listi:");

            oprema[i].Add("Gas");

            for(int ii=0;i<oprema.Length; i++)
                for(int j=0;j<oprema[ii].Capacity;j++)
                    Console.Write(" " + oprema[ii][j]);
            //foreach (ArrayList s in oprema[i])
            //{
            //    foreach (var ss in s) Console.Write(" " + ss);
            //}

            //Console.WriteLine("Unesi novo godiste auta:");
            //godiste[i] = Console.ReadLine();
        }


    }
}
