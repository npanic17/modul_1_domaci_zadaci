﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_2.cas_domaci.src.DodatniZadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList sifra = new ArrayList { "001", "002", "003" };
            ArrayList naslov = new ArrayList { "Prodajem punta", "Audi ko casa", "Uvoz iz svice" };
            ArrayList cena = new ArrayList { "1100", "2000", "3000" };
            ArrayList godiste = new ArrayList { "2000", "2001", "2010" };
            ArrayList oprema1 = new ArrayList { "ABS", "ESP", "Alarm", "AirBag", "AutoKlima" };
            ArrayList oprema2 = new ArrayList { "ABS", "ESP", "Alarm", "AirBag", "Klima" };
            ArrayList oprema3 = new ArrayList { "ABS", "Maglenke", "Alarm", "AirBag", "Servo" };

            ArrayList[] oprema = new ArrayList[3] { new ArrayList(), new ArrayList(), new ArrayList() };
            oprema[0].Add(oprema1);
            oprema[1].Add(oprema2);
            oprema[2].Add(oprema3);

            //OglasiPolovniAuto.IspisSvihOglasa(sifra, naslov, cena, godiste, oprema);
            //OglasiPolovniAuto.IspisSvihOglasaPoGodistu(sifra, naslov, cena, godiste, "2010");
            //OglasiPolovniAuto.IspisDetaljaJednogOglasa(sifra, naslov, cena, godiste, oprema, "001");

            //OglasiPolovniAuto.IzmenaOglasaOsnovno(sifra, naslov, cena, godiste, "001");
            //OglasiPolovniAuto.IspisDetaljaJednogOglasa(sifra, naslov, cena, godiste, oprema, "001");

            OglasiPolovniAuto.IzmenaOglasaOpreme(sifra, oprema, "001");
            //OglasiPolovniAuto.IspisDetaljaJednogOglasa(sifra, naslov, cena, godiste, oprema, "001");

            Console.ReadKey();
        }
    }
}
