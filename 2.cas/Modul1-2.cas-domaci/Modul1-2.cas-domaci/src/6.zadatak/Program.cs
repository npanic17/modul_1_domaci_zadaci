﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Modul1_2.cas_domaci.src._6.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {

            string NASTAVNIK = "1,Petar,Petrović,Docent\n2,Jovan,Jovanović,Docent\n3,Marko,Marković,Asistent\n4,Nikola,Nikolić,Redovni Profesor\n5,Lazar,Lazić,Asistent";
            string PREDMETI = "1,Matematika\n2,Fizika\n3,Elektrotehnika\n4,Informatika";
            string STUDENTI = "1,E1 01/2016,Jovanović, Zarko,Loznica\n2,E2 02/2015,Prosinečki,Strahinja,Novi Sad\n3,E2 33/2016,Savić,Nebojša,Inđija\n4,SW 36/2013,Sekulić,Ana,Niš\n5,E2 157/2013,Nedeljković,Vuk,Novi Sad\n6,E1 183/2013,Klainić,Jovana,Sombor\n7,E2 44/2015,Bojana,Panić,Sr.Mitrovica";

            //Nastavnici a = new Nastavnici();
            //Predmeti b = new Predmeti();
            //a.Ucitaj(NASTAVNIK);
            //a.Ispis();
            //a.IspisPoNastavnikId(3);

            //b.Ucitaj(PREDMETI);
            //b.Ispis();
            //b.IspisPoPredmetId(2);
            Studenti c = new Studenti();
            c.Ucitaj(STUDENTI);
            c.Ispis();
            //c.IspisPoStudentId(3);
            c.IspisPoSmeru("E2");
            Console.ReadKey();
        }
            
        
    }
}
