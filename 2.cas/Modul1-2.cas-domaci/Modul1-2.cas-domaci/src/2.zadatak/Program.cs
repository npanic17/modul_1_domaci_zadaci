﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Modul1_2.cas_domaci.src._2.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {

            string NASTAVNIK = "1,Petar,Petrović,Docent\n2,Jovan,Jovanović,Docent\n3,Marko,Marković,Asistent\n4,Nikola,Nikolić,Redovni Profesor\n5,Lazar,Lazić,Asistent";
            string PREDMETI = "1,Matematika\n2,Fizika\n3,Elektrotehnika\n4,Informatika";

            Nastavnici a = new Nastavnici();
            Predmeti b = new Predmeti();
            a.Ucitaj(NASTAVNIK);
            a.Ispis();
            a.IspisPoNastavnikId(3);

            b.Ucitaj(PREDMETI);
            b.Ispis();
            b.IspisPoPredmetId(2);
            
            Console.ReadKey();
        }
            
        
    }
}
