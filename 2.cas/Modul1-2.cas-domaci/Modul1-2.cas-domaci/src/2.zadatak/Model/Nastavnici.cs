﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1_2.cas_domaci.src._2.zadatak
{
    public class Nastavnici
    {
        private int[] NastavnikId = new int[5];
        private string[] Ime = new string[5], Prezime = new string[5], Zvanje = new string[5];

        public void Ucitaj(string s)
        {
            string[] ss = s.Split('\n');
            string[] sss = null;
            for (int i = 0; i < ss.Length; i++)
            {
                sss = ss[i].Split(',');
                NastavnikId[i] = Int32.Parse(sss[0]);
                Ime[i] = sss[1];
                Prezime[i] = sss[2];
                Zvanje[i] = sss[3];
            }
        }

        public void Ispis()
        {
            for (int i=0; i<NastavnikId.Length;i++)
            {
                Console.WriteLine("NastavnikId:" + NastavnikId[i]);
                Console.WriteLine("Ime:" + Ime[i]);
                Console.WriteLine("Prezime:" + Prezime[i]);
                Console.WriteLine("Zvanje:" + Zvanje[i]);
                Console.WriteLine();
            }
        }

        public void IspisPoNastavnikId(int Id)
        {
            for (int i = 0; i < NastavnikId.Length; i++)
            {
                if (i == (Id-1))
                {
                    Console.WriteLine("NastavnikId:" + NastavnikId[i]);
                    Console.WriteLine("Ime:" + Ime[i]);
                    Console.WriteLine("Prezime:" + Prezime[i]);
                    Console.WriteLine("Zvanje:" + Zvanje[i]);
                    Console.WriteLine();
                }
            }
        }


    }
}
